/******************************************************************************/
/******************************************************************************/
/*  Controller: null 
/*  Template: /client/example/insight
/******************************************************************************/
/******************************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
// Created
/*******************************/
Template.adduser.created = function () {
  //Creates instance
  this.pHappy = CreatePageHappy({
    pagerMasterId: 'insight',
    collection: 'People',
    pageSize: 10,
    catchLimit: 20000,
    //insight just create some
    //sessions for the count
    insight: true,
    querySort: {
      number: 1
    },
  });

  //People Colection Counter
  Meteor.subscribe('peopleCounter');
};


/*******************************/
// Destroyed
/*******************************/
Template.adduser.destroyed = function () {

  this.pHappy.destroy();
};


/***************************************************************/
/* Template Helpers */
/***************************************************************/
Template.adduser.helpers({
  pHappyContext: function () {
    var pHappy = Template.instance().pHappy;
    return pHappy;
  },
  subReady: function () {
    var pHappy = Template.instance().pHappy;
    return pHappy.subReady();
  },
  pHappyData: function () {
    var pHappy = Template.instance().pHappy;
    return pHappy.render(); 
  },
  //*-----------------------------*/
  /// Shared Collection
  /*-----------------------------*/
  pHappyShared: function () {
    // var cursor = Pager.find(),
    //     currentPage = Session.get('pHappy_currentPage')+1;
    // return cursor.map(function (doc) {
    //   //Prbly a better way, but this works, no need to dwell.
    //   var pageNum = doc.number * 0.10 !== currentPage ? Math.floor(doc.number * 0.10)+1 : currentPage,
    //       color = pageNum === currentPage ? '#d95c5c' : '#00b5ad';
    //   return {
    //     _id: doc._id,
    //     number: doc.number,
    //     first: doc.firstName,
    //     last: doc.lastName,
    //     pageNum: pageNum,
    //     color: color
    //   };
    // });
  },
  //*-----------------------------*/
  /// Counters
  /*-----------------------------*/
  localCol: function () {
    return Session.get('pHappy_localCount');
  },
  shareCol: function () {
    return Session.get('pHappy_pagerCount');
  },
  //*-----------------------------*/
  /// Page Status
  /*-----------------------------*/
  pagesLoaded: function () {
    var pagesLoaded = Session.get('pHappy_pagesLoaded'),
        currentPage = Session.get('pHappy_currentPage');
    //Simple map to extract/display info
    var pageInfo = _.map(pagesLoaded, function (num) {
      if (num === currentPage) {
        return {
          //Current
          num: num,
          color: '#d95c5c'
        };
      }else{
        return {
          //Not current
          num: num,
          color: '#00b5ad'
        };
      }
    });
    //Everyone likes a sorted list
    return _.sortBy(pageInfo, function (val) {
      return val.num;
    });
  }
});


Template.adduser.events({
  'click .addUser': function () {
      var createFakeUser = function () {
        var name = fakeFixture.getName();
        return {
          number: Math.floor((Math.random() * 20) + 1),
          firstName: name,
        };
      };

      var initFakeUsers = function (number) {
        for (var i = 0; i < number; i++) {
          var user = createFakeUser();
          People.insert(user);
          console.log(user.firstName + '--User Created');
          console.log(user.number + '--User Created');
        }
      };

      initFakeUsers(1); 
  }
});

var fakeFixture = {
  getPhone: function () {
    return Math.floor(1000000000 + Math.random() * 900000000);
  },
  getName: function() {
    var namesLength = this.names.length,
        name = this.names[Math.floor(Math.random() * namesLength)];
    return this.capitalize(name);
  },
  getWord:  function(min, max) {
    var length = this.wordLengths[Math.floor(Math.random() * 16)];
    if(min && (length < min)) length = min;
    if(max && (length > max)) length = max;
    var word = '';
    for(var i = 0; i < length; ++i) {
      var count = this.syllabeCounts[Math.floor(Math.random() * 16)];
      word += this.syllabes[Math.floor(Math.random() * count)];
    }
    return word;
  },
  getLocation: function () {
    var location = this.fromArray(this.cities);
    return {
      city: location[0],
      state: location[1]
    };
  },
  getSentence: function(length) {
    if(!length) {
      var length = 4 + Math.floor(Math.random() * 8);
    }
    var ending = (Math.random() < 0.95) ? '.' : (Math.random() < 0.5) ? '!' : '?';
    var result = this.getWord();
    result = result.slice(0,1).toUpperCase() + result.slice(1).toLowerCase();
    for(var i = 1; i < length; ++i) {
      result += ' ' + this.getWord();
    }
    return result + ending;
  },
  getParagraph: function(length) {
    if(!length) {
      length = 6 + Math.floor(Math.random() * 8);
    }
    var result = this.getSentence();
    for(var i = 1; i < length; ++i) {
      result += ' ' + this.getSentence();
    }
    return result;
  },
  getDomain: function() {
    return this.getWord(2) + this.domains[Math.floor(Math.random() * 8)];
  },
  getEmail: function () {
    return (this.getName() + '@' +this.getDomain()).toLowerCase();
  },
  getCompany: function () {
    var name = this.getWord(5, 12);
    return this.capitalize(name);
  },
  getRandomDate: function(start, end) {
    if (!start) {
        start = new Date(1900, 0, 1).getTime();
    } else {
        start = start.getTime();
    }
    if (!end) {
        end = new Date(2100, 0, 1).getTime();
    } else {
        end = end.getTime();
    }
    return new Date(start + Math.random() * (end - start));
    //return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  },
  getBollen: function () {
    return Math.round(Math.random()) === 0 ? true : false;
  },
  capitalize: function(str) {
    return str.slice(0,1).toUpperCase() + str.slice(1).toLowerCase();
  },
  fromArray: function(array) {
    return array[Math.floor(Math.random() * array.length)];
  },
  syllabes: [
  'the','ing','er','a','ly','ed','i','es','re','tion','in','e','con','y','ter','ex','al','de','com','o','di','en','an','ty','ry','u',
  'ti','ri','be','per','to','pro','ac','ad','ar','ers','ment','or','tions','ble','der','ma','na','si','un','at','dis','ca','cal','man','ap',
  'po','sion','vi','el','est','la','lar','pa','ture','for','is','mer','pe','ra','so','ta','as','col','fi','ful','get','low','ni','par','son',
  'tle','day','ny','pen','pre','tive','car','ci','mo','an','aus','pi','se','ten','tor','ver','ber','can','dy','et','it','mu','no','ple','cu',
  'fac','fer','gen','ic','land','light','ob','of','pos','tain','den','ings','mag','ments','set','some','sub','sur','ters','tu','af','au','cy','fa','im',
  'li','lo','men','min','mon','op','out','rec','ro','sen','side','tal','tic','ties','ward','age','ba','but','cit','cle','co','cov','daq','dif','ence',
  'ern','eve','hap','ies','ket','lec','main','mar','mis','my','nal','ness','ning','nu','oc','pres','sup','te','ted','tem','tin','tri','tro','up',
  ],
  wordLengths: [
    1, 1,
    2, 2, 2, 2, 2, 2, 2,
    3, 3, 3, 3,
    4, 4,
    5
  ],
  syllabeCounts: [
    10,15,20,25,
    30,35,40,45,
    50,75,100,125,
    150,175,175,175
  ],
  names: [
    'Abigail','Alice','Amelia','Angelina','Ann',
    'Ashley','Avery','Barbara','Brianna','Camila',
    'Chloe','Dorothy','Elizabeth','Ella','Emily',
    'Emma','Fiona','Florence','Gabrielle','Haley',
    'Hannah','Isabella','Jasmine','Jennifer','Jessica',
    'Juliette','Kate','Leah','Lily','Linda',
    'Lea','Madison','Makayla','Margaret','Maria',
    'Mariana','Mary','Megan','Mia','Olivia',
    'Patricia','Rachel','Samantha','Sarah','Sophie',
    'Susan','Taylor','Valeria','Victoria','Zoe',
    'Alexander','Anthony','Benjamin','Brandon','Carter',
    'Charles','Charlie','Christian','Christopher','Daniel',
    'David','Deven','Dylan','Elijah','Eric',
    'Ethan','Felix','Gabriel','George','Harry',
    'Hudson','Hunter','Jack','Jacob','James',
    'Jason','Jayden','Jeremiah','John','Joseph',
    'Joshua','Justin','Kevin','Liam','Logan',
    'Lucas','Matthew','Michael','Neil','Noah',
    'Oliver','Owen','Raphael','Richard','Robert',
    'Ryan','Samuel','Thomas','Tyler','William'
  ],
   domains: [
  '.net', '.org', '.edu', '.com',
  '.com', '.com', '.com', '.com',
  ],
  cities: [
    ['New York', 'New York'],
    ['Los Angeles', 'California'],
    ['Chicago', 'Illinois'],
    ['Houston', 'Texas' ],
    ['Philadelphia', 'Pennsylvania'] ,
    ['Phoenix', 'Arizona'],
    ['San Diego', 'California'],
    ['Dallas', 'Texas'],
    ['San Antonio', 'Texas' ],
    ['Detroit', 'Michigan'],
    ['San Jose', 'California'],  
    ['Indianapolis', 'Indiana'], 
    ['San Francisco', 'California'], 
    ['Jacksonville', 'Florida'], 
    ['Columbus', 'Ohio '], 
    ['Austin', 'Texas'], 
    ['Memphis', 'Tennessee'],  
    ['Baltimore', 'Maryland'], 
    ['Milwaukee', 'Wisconsin'],  
    ['Boston', 'Massachusetts'], 
    ['Charlotte', 'North Carolina'],
    ['El Paso', 'Texas'],  
    ['Washington', 'D.C.'],  
    ['Seattle', 'Washington'],
    ['Fort Worth', 'Texas'], 
    ['Denver', 'Colorado'],  
    ['Nashville-Davidson', 'Tennessee'], 
    ['Portland', 'Oregon'],  
    ['Oklahoma City', 'Oklahoma'],
    ['Las Vegas', 'Nevada'], 
  ]
};