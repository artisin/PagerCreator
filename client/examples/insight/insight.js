/******************************************************************************/
/******************************************************************************/
/*  Controller: null 
/*  Template: /client/example/insight
/******************************************************************************/
/******************************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
// Created
/*******************************/
Template.insight.created = function () {
  //Creates instance
  this.pHappy = CreatePageHappy({
    pagerMasterId: 'insight',
    collection: 'People',
    pageSize: 10,
    catchLimit: 20000,
    //insight just create some
    //sessions for the count
    insight: true,
    querySelector: {
      "email.verified": true
    },
    querySort: {
      number: 1
    },
  });

  //People Colection Counter
  // Meteor.subscribe('peopleCounter');
};


/*******************************/
// Destroyed
/*******************************/
Template.insight.destroyed = function () {

  this.pHappy.destroy();
};

/***************************************************************/
/* Template Helpers */
/***************************************************************/
Template.insight.helpers({
  pHappyContext: function () {
    var pHappy = Template.instance().pHappy;
    return pHappy;
  },
  subReady: function () {
    var pHappy = Template.instance().pHappy;
    return pHappy.subReady();
  },
  pHappyData: function () {
    var pHappy = Template.instance().pHappy;
    return pHappy.render(); 
  },
  //*-----------------------------*/
  /// Shared Collection
  /*-----------------------------*/
  pHappyShared: function () {
    // var cursor = Pager.find(),
    //     currentPage = Session.get('pHappy_currentPage')+1;
    // return cursor.map(function (doc) {
    //   //Prbly a better way, but this works, no need to dwell.
    //   var pageNum = doc.number * 0.10 !== currentPage ? Math.floor(doc.number * 0.10)+1 : currentPage,
    //       color = pageNum === currentPage ? '#d95c5c' : '#00b5ad';
    //   return {
    //     _id: doc._id,
    //     number: doc.number,
    //     first: doc.firstName,
    //     last: doc.lastName,
    //     pageNum: pageNum,
    //     color: color
    //   };
    // });
  },
  //*-----------------------------*/
  /// Counters
  /*-----------------------------*/
  localCol: function () {
    return Session.get('pHappy_localCount');
  },
  shareCol: function () {
    return Session.get('pHappy_pagerCount');
  },
  //*-----------------------------*/
  /// Page Status
  /*-----------------------------*/
  pagesLoaded: function () {
    var pagesLoaded = Session.get('pHappy_pagesLoaded'),
        currentPage = Session.get('pHappy_currentPage');
    //Simple map to extract/display info
    var pageInfo = _.map(pagesLoaded, function (num) {
      if (num === currentPage) {
        return {
          //Current
          num: num,
          color: '#d95c5c'
        };
      }else{
        return {
          //Not current
          num: num,
          color: '#00b5ad'
        };
      }
    });
    //Everyone likes a sorted list
    return _.sortBy(pageInfo, function (val) {
      return val.num;
    });
  }
});
