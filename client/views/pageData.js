/******************************************************************************/
/******************************************************************************/
/*  Controller: pageData 
/*  Template: /client/views/templateLocation
/******************************************************************************/
/******************************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
  Session.set('hope', 0);
/*******************************/
// Created
/*******************************/
Template.pageData.created = function () {
  this.pager = PagerCreator({
    pagerMasterId: 'people1',
    collection: 'People',
    pageSize: 25,
    catchLimit: 500,
    cleanTrigger: 400,
    autoUpdatePreFetch: true,
    querySpotlight: {
//       addCustomFields: true,
//       //Only Functions
//       customFields: {
// //        Name of new field
//         odd: {
//           //What you want the field to equal
//           equals: {
//             true: 'yes',
//             false: 'no'
//           },
//           //The function to add it
//           funk: function (doc) {
//             return doc.number % 2 === 0;
//           }
//         },
//         // cool: {
//         //   equals: 'Hell Yeah',
//         //   funk: function (doc) {
//         //     return doc.firstName === 'Mia';
//         //   }
//         // }
//       },



      // // customFunctions: true,
      // //Must have a bollen outcome
      // funk: function (doc) {
      //   return (doc.firstName.length >= 6);
      // },
      // // funk2: function (doc) {
      // //   doc.name
      // // }



      // contains: true,
      // containsList: ['Mia', 1, 'Ryan']
      // containsAllList: ['Mia', 1, 'Ryan']
    },
    // querySelector: {
    //   firstName: 'John'
    // },
    querySort: {
      number: 1
    },
  });
};

/*******************************/
// Rendered
/*******************************/
Template.pageData.rendered = function () {
  var _PC = this;
  // $('.pageData_Container').scrollTop(1)

  // console.log($('.pageData_Container').scroll())
  // Tracker.autorun(function () {
  //   console.log($('.pageData_Container').scroll(function () {
  //   console.log($(this).scrollTop() + $(this).innerHeight())
  //     console.log($(this).scrollTop())
  //     if ($(this).scrollTop() === 1) {

  //     };
  //   }))
  // });
};

/*******************************/
// Destroyed
/*******************************/
Template.pageData.destroyed = function () {
  console.log('DESTRoy')
  this.pager.destroy();
};


/***************************************************************/
/* Template Helpers */
/***************************************************************/
Template.pageData.helpers({
  pager: function () {
    var pager = Template.instance().pager;
    return pager;
  },
  subReady: function () {
    var pager = Template.instance().pager;
    return pager.subReady();
  },
  page: function () {
    var pager = Template.instance().pager;
    // console.log('RENDER+++++++++++++++++++++++++++++++++++++++++')
    // pager.render();
    var collection = pager.render(); 

    return collection.map(function (doc) {
     return {
        firstName: doc.firstName,
        number: doc.number,
        highlight: doc.highlight === true ? 'highlight' : '',
        odd: doc.odd
      };
    });
  },
});


/***************************************************************/
/* Template Events */
/***************************************************************/
Template.pageData.events({
  'click .settings': function (e, tmpl) {
    e.preventDefault();
    console.log('mes')
    var pager = Template.instance().pager;
    pager.changeSetting('pageSize', 5);
  },
  'click .ten': function (e, tmpl) {
    e.preventDefault();
    var pager = Template.instance().pager;
    pager.changeSetting('pageSize', 10);
  },
  'click .highlightEven': function (e) {
    e.preventDefault();
    var pager = Template.instance().pager;
    pager.changeSetting('querySpotlight', {
      customFunctions: true, 
      funk: function (doc) {
       return doc.number % 2 === 0;
      }
    });
  },
  'click .highlightOdd': function (e, tmpl) {
    e.preventDefault();
    var pager = Template.instance().pager;
    pager.changeSetting('querySpotlight', {
      customFunctions: true,
      funk: function (doc) {
       return doc.number % 2 === 1;
      }
    });
  },
  'click .unHighlight': function (e, tmpl) {
    e.preventDefault();
    var pager = Template.instance().pager;
    pager.changeSetting('querySpotlight', {});
  },
});



