CreatePageHappy = function (options) {
  function HPager () {};
  HPager.prototype = pagerCreatorProtoype;
  var f = new HPager();
  f.init(options);
  return f;
};

_PC = null;
LocalCol = {};
pagerClientObserver = {};
var pagerCreatorProtoype = {
  init: function (options) {
    console.log('INIT RAN')
    //////////************************ uncomment
    // var _PC;
    // var pagerClientObserver = null;
    var defaults = {
      pagerMasterId: undefined,
      collection: undefined,
      pageSize: 25,
      currentPage: 0,

      /*-----------------------------*/
      /// Auto Update
      /*-----------------------------*/
      //Will update records on add/change/remove
      autoUpdate: true,
      //Throttle time.
      //As such it will only allow autoUpate to be called once every (x)
      //which in turn will poll the collection for changes
      autoUpdatePollPeriod: 3000,


      /*-----------------------------*/
      /// PreFetch
      /*-----------------------------*/
      //Pre-fetches next pages
      preFetch: false,
      //How many sets will be prefetched
      //Default 1 === preFetch neighboors
      preFetchDeviation: 6,

      //** if preFetch false -> need to be false
      preFetchBatch: false,
      preFetchBatchSize: 3,

      //Defualt 0 -> will not run at 0,
      // preFetchBatchTrigger: 0, 


      preFetchAll: false,
      //Default -> 0
      preFetchAllBatch: 1000,
      //Default -> 0
      preFetchAllBatchTimeIntv: 0,

      ///**** preFetchDeviation > preFetchBatch
      //** if preFetch false -> need to be false
      //Alright we about to get a little crazy here.
      //So if a document is changed and now falls withing the preFetch perview
      //but not the current page, it will be added although when the user goes
      //to that page the data will reflect that of the old data untill the 
      //autoUpatePollPeriod has expired and the call is made to the server which 
      //it will then realize the data is incorrect and procced to change that data. 
      //So everything works. As expexted.
      //Nevertheless, if true this it will proceed to check all prefetched pages
      //to make sure that the data is correct. 
      autoUpdatePreFetch: false,
      //Throttle invervolt
      autoUpdatePreFetchDebounce: 3000,
      //Will intelegently check in batches, so lets say you got the following
      //prefetched [1,2,3,4,5,6], [11,12,13,14], [33,32,34,36,37]
      //it will prefetch theses groups thus making only three calls to theee server yo.
      preFetchCheckBatch: false,
      //
      preFetchCheckBatchSize: 10, 





      //Based on pageSize (2*25pageSize = 50)
      //Depermins the inital sub limit, once data is then catched
      //the sub limit will defualt to the catchLimit
      initSubLoad:  2,

      //These index will remain in the catch and will not be cleaned
      //last indes if last arg === true
      // ensureCatch: [0, 1, 2, true],
      ensureCatch: [0, 1, 2, true],



      totalRecords: 0,
      users: Meteor.user !== undefined ? true : false,
      querySelector: {},
      querySort: {
        _id: -1
      },
      queryFields: {},
      queryFilter: {},
      spotlight: {},

      queryRange: true,
      method: 'getPagerAsync',
      pass: 0,
      currentCatchSize: 0,

      cleanCollectionCatch: false,
      catchLimit: 500,

      //Size is based on pageSize
      //Such as (pageSize: 10, catchCleanerSize: 2) = 20
      //So when the catch limit is reached 20 will be removes
      ////FIX withi limit
      catchCleanerSize: 2,

      //Determins how we will clean catch
      //Two options:
      //page - cleans the pages furthes away from current
      //timestamp - cleanst the oldest
      cleanCatchBy: 'page',

      cleanCollectionOnLoad: false,

      //Ref to all the trackers since they need to be stoped
      //on the destroyed call
      trackers: [],

      //
      insight: false,
      //Will override any defualt settings
      overRide: false
    };

    //Extend this, to merge
    _.extend(this, defaults, options);
    _PC = this;
    _PC.initSetUpComplete = new ReactiveVar(false);




    //*---------------------------------------------*/
    // Init Setup Process
    /*---------------------------------------------*/
    ASQ()
      //Create New Local Collection
      .then(function createLocalCol (done) {
        _PC.ID = _PC.pagerMasterId;
        redis[_PC.ID] = new Miniredis.RedisStore;
        // LocalCol[_PC.ID] = new Mongo.Collection(_PC.ID, {connection:null});
        done();
      })
      //Run init setup
      .then(function initSetup(done) {
        var sq = ASQ();
        sq
          .seq(_PC.initSetup())
          .then(function () {
            //Parent done
            done();
          });
      })
      .then(function update (done) {
        var sq = ASQ();
        sq
          .seq(_PC.update())
          .then(function () {
            //parent done
            done();
          });
      })
      .then(function (done) {
        _PC.initSetUpComplete.set(true);
        //Activates Trackers And such;
        done(pagerWatch());
      })
      .then(function (done) {
        //First pass though data generation is called here,
        //otherwise it is called in the update funk
        _PC.renderDataGenerator();
        done();
      })
      .or(function (err) {
        console.warn('Init Error!');
        console.warn(err);
      });


      function pagerWatch () {
      /*---------------------------------------------*/
      // Auto Run Trackers
      /*---------------------------------------------*/
      

      /*-----------------------------*/
      /// Pre Fetch Catch Check
      /*-----------------------------*/
      if (_PC.autoUpdatePreFetch) {


        //Watches list which is created at _PC.autoUpdateCheck
        var callPreFetchCheck = function (checkList) {
          return _PC.preFetchCheck(checkList);
        };
        var preFetchCheck = _.debounce(callPreFetchCheck, _PC.autoUpdatePreFetchDebounce);
        var t1 = Tracker.autorun(function () {
          var checkList = _PC.preFetchCheckList.get(),
              fetchCheckComplete = _PC.preFetchCheckComplete.get();
          //Don't call if in progress or with an empty list
          if (fetchCheckComplete && !_.isEmpty(checkList) && !_PC.tempPreFetchCheckBlock.get()) {
              preFetchCheck(checkList);
          }
        });
        _PC.trackers.push(t1);



        //Resets Block (refer to autorun bellow)
        var resetBlock = function () {
          return Meteor.setTimeout(function () {
            _PC.tempPreFetchCheckBlock.set(false);
          },  _PC.autoUpdatePreFetchDebounce*2);
        };
        //Watches preFetchBlock, this is set to true if there is a change
        //in pager settings since we do not want to check ids that are in flux
        var t2 = Tracker.autorun(function () {
          var tempBlock = _PC.tempPreFetchCheckBlock.get();
          if (tempBlock) {
            resetBlock();
          }
        });
        _PC.trackers.push(t2);

      }


      /*-----------------------------*/
      /// Observer Selector
      /*-----------------------------*/
      var t3 = Tracker.autorun(function () {
        // var catchedDataList = _PC.catchedDataList.get();
        if (_PC.user) {
          var userId = Meteor.userId();
          _PC.obvSelector.set({$and: [
            {pagerMasterId: _PC.pagerMasterId},
            {userId: userId}
          ]});
        }else{
          //No users
          // _PC.obvSelector.set({pagerId: {$in: catchedDataList}});
          _PC.obvSelector.set({pagerMasterId: _PC.pagerMasterId});
        }
      });
      _PC.trackers.push(t3);
      
      /*-----------------------------*/
      /// Observer Limit Setter
      /*-----------------------------*/
      //Observer Sub Init limit
      // _PC.obvSubLimit = new ReactiveVar(_PC.initSubLoad * _PC.pageSize.get());

      //Once the first set is catched, subLimit will
      //be changed to catch size
      var t4 = Tracker.autorun(function () {
        var pagesCatched = _PC.pagesCatched.get(),
            pageSize = _PC.pageSize.get();

        if (!_PC.preFetch) {
          _PC.obvSubLimit.set(_PC.catchLimit);
        }else{
          _PC.obvSubLimit.set(pageSize);
          if (pagesCatched.length > _PC.initSubLoad) {
            _PC.obvSubLimit.set(_PC.catchLimit);
          }
        }


      });
      _PC.trackers.push(t4);




      /*-----------------------------*/
      /// Sub Handle for Pager
      /*-----------------------------*/
      var t5 = Tracker.autorun(function () {
        _PC.subHandle = Meteor.subscribe('pager', 
          _PC.obvSelector.get(), 
          _PC.querySort,
          _PC.obvSubLimit.get()
        );
      });
      _PC.trackers.push(t5);


      /*-----------------------------*/
      /// Cleans collection
      /*-----------------------------*/
      if (_PC.cleanCollectionCatch) {
        // var cleanCatch = function () {
        //     _PC.cleanComplete.set(false);
        //     _PC.cleanCatch();
        // };
        // /**
        //  * When the catch limit has been hit this funk controlls the catch
        //  * cleaner process
        //  */
        // var t6 = Tracker.autorun(function () {
        //   var currentCatchSize = LocalCol[_PC.ID].find(_PC.obvSelector.get()).count(),
        //       pageSize = _PC.pageSize.get(),
        //       catchLimit = _PC.obvSubLimit.get(),
        //       cleanTrigger = _PC.cleanTrigger || Math.ceil(catchLimit*0.75),
        //       cleanComplete = _PC.cleanComplete.get();

        //   // console.log(_PC.pass)
        //   //To clean or not to clean. That is the question.
        //   //depends on cleanComplete so shit don't get fucked and people wait their turn.
        //   if (currentCatchSize >= cleanTrigger && cleanComplete && _PC.preFetch) {
        //     cleanCatch();
        //   }

        // });
        // _PC.trackers.push(t6);

          /**
           * **************FIGURE OUT A BETTER WAY ***********************
           * Inital Clean of collection
           * This checks to see if the localCollection does not have any data
           * it should not have. After the check is complete the tracker and 
           * interval will be cleaned.
           * The only real reason this is here for when users refresh their page
           * and by doing so the old data is retained in the local collection
           * with incorrect reffrences. It does not cause any harm per-se
           * but by cleaning it will increase latter speeds, or atleast I think.
           */
          if (!_PC.cleanCollectionOnLoad && _PC.preFetch) {
            // var subsLoaded = new ReactiveVar(false),
            //     initPreFetchComplete = function () {
            //       var loaded = _PC.catchedData.get().length === _PC.ensureCatch.length;
            //       if (_PC.ensureCatch >= _PC.initSubLoad) {
            //         subsLoaded.set(loaded);
            //       }
            //       subsLoaded.set(loaded);
            //     },
            //     resetCatch = function () {
            //       _PC.cleanComplete.set(false);
            //       _PC.cleanCatch(true);
            //     },
            //     //?? do we need???
            //     genInterval = function () {
            //       var inter = _PC.autoUpdatePollPeriod - 2500;
            //       if (0 > inter) {
            //         inter = 10;
            //       }
            //       return 1000;
            //     },
            //     subLoadedCheck = Meteor.setInterval(function () {
            //       initPreFetchComplete();
            //     }, genInterval());

            //   var t7 = Tracker.autorun(function (initClean) {
            //     if (subsLoaded.get()) {
            //       var corretCollectionSize = _PC.catchedData.get().length * (_PC.pageSize.get()),
            //           currentCollectionSize = LocalCol[_PC.ID].find().count(),
            //           //Cleans up tracker and such
            //           cleanTracker = function () {
            //             Meteor.clearInterval(subLoadedCheck);
            //             initClean.stop();
            //           };
            //       //If the calc is not right clean the collection
            //       if (corretCollectionSize !== currentCollectionSize) {
            //         resetCatch();
            //         cleanTracker();
            //       }
            //       cleanTracker();
            //     }
            //   });
            // _PC.trackers.push(t7);
          }


      }

      /*-----------------------------*/
      /// Render Data Generator
      /*-----------------------------*/
      // var pageTracker;
      // Tracker.autorun(function () {
      //   console.log('RENDER Tracker')
      //   var currentPage = _PC.currentPage.get();
      //   console.log(currentPage);
      //   console.log(pageTracker)
      //   if (currentPage !== pageTracker) {
      //     _PC.renderDataGenerator();
      //     pageTracker = currentPage;
      //   }
      // });

      /*-----------------------------*/
      /// Stats -> for insight
      /*-----------------------------*/
      if (_PC.insight) {
        var t8 = Tracker.autorun(function () {
          //Collection
          // Session.set('pHappy_localCount', LocalCol[_PC.ID].find().count());
          Session.set('pHappy_pagerCount', Pager.find().count());
          //Pages
          var pagesLoaded = _PC.catchedData.keys('*'),
              pageNum = _.map(pagesLoaded, function (key) {
                return Number(key.replace( /^\D+/g, ''));
              }),
              current = _PC.currentPage.get();
          Session.set('pHappy_pagesLoaded', pageNum);
          Session.set('pHappy_currentPage', current);
        });
        _PC.trackers.push(t8);
      }


      /*-----------------------------*/
      /// Auto Update Checker
      /// Calls method to check for changes
      /*-----------------------------*/
      var autoUpdateCheck = _.throttle(autoUpdate, _PC.autoUpdatePollPeriod);
      function autoUpdate () {
        return _PC.autoUpdateCheck();
      }
        

      console.log(_PC); 
      // console.log(LocalCol[_PC.ID].find().count())
      /*-----------------------------*/
      /// Pager Observer
      /*-----------------------------*/
      pagerClientObserver[_PC.ID] = Pager.find(
        _PC.obvSelector.get()
      ).observeChanges({
        added: function (id, fields) {
          console.log('ADDED')
            var pagerId = fields.pagerId;

            /**
             * Check to see if the doc exsists in the local redis
             * if not it will add it. If the doc does exsist it
             * will compare the two copies of the doc and check
             * for changes, if they match it will abort if not
             * it will update the redis doc and then abort
             */
            var checkDocExisits = function () {
              var exsits = redis[_PC.ID].exists(pagerId);
              if (exsits) {
                var stringFields = JSON.stringify(fields),
                    redisDoc = redis[_PC.ID].get(pagerId);
                //Compare docs
                if (stringFields !== redisDoc) {
                  //Change the redis doc to reflect new doc
                  redis[_PC.ID].set(pagerId, stringFields);
                  return true;
                }
                return true;
              }else{
                return false;
              }
            };

            var insertDoc = function (done) {
              var insertFields = JSON.stringify(fields);
              // console.log(insertFields);
              redis[_PC.ID].set(pagerId, insertFields);
              done();
            };

            var MGR = ASQ();
            MGR.defer()
              .val(checkDocExisits)
              //Checks is doc is already in local collection
              .then(function isDocInCol (done, exsits) {
                if (exsits) {
                  MGR.abort();
                }else{
                  done();
                }
              })
              //Spotlight
              .then(function spotlight (done) {
                if (_PC.spotlight.all) {
                  var sq = ASQ();
                  sq
                    .seq(_PC.spotlighter(fields))
                    .val(function (spotlightFields) {
                      fields = spotlightFields;
                      //Parent -> Done
                      done();
                    });
                }else{
                  done();
                }
              })
              //In no autoupdate is running
              .then(function noAutoUpdate (done) {
                if (!_PC.autoUpdate) {
                  insertDoc(done);
                  MGR.abort();
                }else{
                  done();
                }
              })
              //Insert new doc into redis
              .then(insertDoc)
              .or(function (err) {
                if (!_PC.overRide) {
                  console.warn('Observer Added Error!');
                  console.warn(err);
                  console.warn('If you would like to remove these error warnings you can add "override: true" to this specific instance');
                }
              });
        },
        changed: function (id) {
          console.log('CHANGED')
          var findSharedDoc = function () {
            var doc = Pager.findOne(id);
            doc = _.omit(doc, '_id');
            return doc;
          };

          var MGR = ASQ();
          MGR.defer()
            .then(function (done) {
              if (!_PC.autoUpdate) {
                MGR.abort();
              }
              done();
            })
            //Get doc info
            .val(findSharedDoc)
            .then(function changeDoc(done, doc) {
              //Only update doc if found
              if (_.isEmpty(doc)) {
                MGR.abort();
                done();
              }
              done(doc);
            })
            .then(function spotlight (done, fields) {
              //Apply spotlight filters
              if (_PC.spotlight.all) {
                var sq = ASQ();
                return sq
                  .seq(_PC.spotlighter(fields))
                  .val(function (newFields) {
                    //Parent done
                    done(newFields);
                  });
              }else{
                done(fields);
              }
            })
            //Update redis
            .then(function updateDoc (done, doc) {
              var rId = doc.pagerId,
                  formatedDoc = JSON.stringify(doc);
              //Overright redis doc with new doc info
              redis[_PC.ID].set(rId, formatedDoc);
              done();
            })
            //Upate the id list for the current page
            .then(function (done) {
              autoUpdateCheck();
              done();
            })
            .or(function (err) {
              if (!_PC.overRide) {
                console.warn('Observer Changed Error!');
                console.warn(err);
                console.warn('If you would like to remove these error warnings you can add "override: true" to this specific instance');
              }
            });
        },
        removed: function () {
          console.log('REMOVED')
          // console.log(arguments)
          if (_PC.autoUpdate) {
            //Poll the collection to check for changes - throttled
            autoUpdateCheck();
          } 
        }, 
      });

    }
  },
  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Update Params for method calls
  // @return - params
  /*--------------------------------------------------------------------------*/
  updateParams: function () {
    var _PC = this,
        params = {
          collection: _PC.collection,
          pagerMasterId: _PC.pagerMasterId,
          selector: _PC.querySelector,
          sort: _PC.querySort,
          fields: _PC.queryFields,
          filter: _PC.queryFilter,
          limit: _PC.obvSubLimit.get(),
          pageSize: _PC.pageSize.get(),
          currentPage: _PC.currentPage.get(),
          users: _PC.users,
        };
    if (_PC.cleanCatchBy === 'timestamp') {params.pagerTimestamp = +new Date();}
    
    if (_PC.queryRange) {
      params.queryRange = true;
      //No need to send lastRange due to inharent limit
      if (_PC.pass >= 1) {
        var pageSize = params.pageSize,
            currentPage = params.currentPage,
            key = "p-"+(currentPage-1);
        params.lastRangeId = _.first(_PC.catchedData.lrange(key, (pageSize-1), pageSize));
      }
    }
    console.log(params)

    return params;
  },

  //*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Spotlighter
  /*--------------------------------------------------------------------------*/
  spotlighter: function (fields) {
    var _PC = this,
        originVal = _PC.spotlight.get(), 
        settings = _.pluck(originVal, 'current', 'all', 'singlePage', 'page'),
        spotlight = _.omit(originVal, 'current', 'all', 'singlePage', 'page');
// debugger
    /*-----------------------------*/
    /// Config
    /*-----------------------------*/
    var configure = function () {

      //Setters//
      var spotlightType = null;
      //Custom function
      if (spotlight.customFunctions) {
        spotlightType = customFunctions;
      }
      //_.contains
      if (spotlight.contains) {
        spotlightType = contains;
      }
      //Disable all spotlight
      if (_.isEmpty(spotlight) && settings.all) {
        spotlightType = none;
      }

      //Checkers//
      //Simple check to make sure we have atleast one truth
      var oneTruth = _.map(spotlight, function (val) {
        return val;
      });
      //If there is not one truthy
      if (!_.contains(oneTruth, true) && !_.isEmpty(spotlight)) {
        //Check if only funks
        var isFunks = _.map(oneTruth, function (val) {
          return _.isFunction(val);
        });
        //True if only funks
        if (_.every(isFunks, _.identity)) {
          spotlightType = customFunctions;
        }else if(_.isObject(spotlight.customFields)){
          spotlightType = null;
        }else{
          throw 'Sorry but I could not determin what you are trying to do. Try to speficy what your are using; such as "customeFunctions: true" otherwise I get all confuzed.';
        }
      }
      
      if (spotlight.contains && spotlight.customFunctions) {
        throw 'Unfortunately, at this time you have you can not use both custom functions and contains. Although, you can create a custom function that uses _.contains and that should worlk swell.';
      }
      return spotlightType;
    };


    //*-----------------------------*/
    /// Custom Function
    /*-----------------------------*/
    var customFunctions = function (fields) {
      var funkExsists = _.map(spotlight, function (val) {
        return _.isFunction(val);
      });
      //Throw err if no funk
      if (!_.contains(funkExsists, true)) {
        throw 'No functions found, yet you specified that this is what you want to do. Make is so, and add a function with a boolean outcome';
      }

      /**
       * Runs through spotlight functions
       * @return - modified fields to reflect that of spotlight
       */
      var funkCheck = _.map(spotlight, function (val, indvFunk) {
        if (_.isFunction(val)) {
          return spotlight[indvFunk](fields);
        } 
      });
      //Clean
      funkCheck = _.without(funkCheck, undefined);
      //Set the new value for spotlight
      fields.spotlight = _.every(funkCheck, _.identity);
      //->
      return fields;
    };


    //*-----------------------------*/
    /// Underscore Contains
    /*-----------------------------*/
    var contains = function (fields) {
      var list,
          listType = (function () {
            if (spotlight.containsList !== undefined) {
              list = spotlight.containsList;
              return 'one';
            }else if(spotlight.containsAllList !== undefined){
              list = spotlight.containsAllList;
              return 'all';
            }
          })();
      //Throw error if no list
      if (list === undefined) {
        throw 'No containsList or containsAllList field found. Make is so, in order to go.';
      }
      var containsCheck = _.map(list, function (val) {
        if (_.contains(fields, val)) {
          return true;
        }
        return false;
      });

      //Contains one
      if (listType === 'one') {
        if (_.contains(containsCheck, true)) {
          fields.spotlight = true;
        }
      }
      //Contains All
      if (listType === 'all') {
        if (_.every(containsCheck, _.identity)) {
          fields.spotlight = true;
        }
      }
      return fields;
    };


    //*-----------------------------*/
    /// Custome Fields
    /*-----------------------------*/
    var customFields = function (field) {
      var funks = spotlight.customFields;
      if (funks === undefined) {
        throw 'No customFields object found! Make it so, to procced.';
      }

      var checkFunks = function () {
        _.each(funks, function (val, index, list) {
          if (val.equals === undefined) {
            throw 'No "equals" field found in your customFields! Make it so, to procced.';
          }
          var hasFunk = _.map(val, function (indVal) {
            if (_.isFunction(indVal)) {
              return true;
            }
          });
          //Clean
          hasFunk = _.compact(hasFunk);
          if (!_.contains(hasFunk, true)) {
            throw 'No function found in one of your customFields! Make it so, to procced.';
          }
        });
        return funks;
      };

      var runFunks = function (funkObj, field) {

        _.each(funkObj, function (obj, newField) {
          field[newField] = null;

          //Grab funk
          var conditionFunk = _.map(obj, function(subFunk) {
            if (_.isFunction(subFunk)) {
              return subFunk;
            }
          });
          conditionFunk = _.first(_.compact(conditionFunk));
          //Grab condition
          var conditionToApply = _.map(obj, function (subCondition) {
            if (!_.isFunction(subCondition)) {
              return subCondition;
            }
          });
          conditionToApply = _.first(_.compact(conditionToApply));


          var equalsType = null;
          //One outcome check
          if (_.isString(conditionToApply)) {
            equalsType = 'one';
          }
          //Boolean outcome check
          if (_.isObject(conditionToApply)) {
            var cond = conditionToApply;
            if (cond.true === undefined) {
              throw 'Shit son, you gotta have a truth outcome';
            }
            if ( cond.false === undefined) {
              throw 'Shit son, you gotta have a false outcome.';
            }
            equalsType = 'boolean';
          }

          //Funk outcome
          var funkOutcome = conditionFunk(field);
          //
          if (equalsType === 'boolean') {
            if (funkOutcome) {
              field[newField] = conditionToApply.true;
            }else{
              field[newField] = conditionToApply.false;
            }
          }
          //
          if (equalsType === 'one') {
            if (funkOutcome) {
              field[newField] = conditionToApply;
            }
          }
        });
        return field;
      };

      return ASQ()
        .val(checkFunks)
        .val(function (funkObj) {
          return runFunks(funkObj, field);
        })
        .or(function (err){
          console.warn('Custome Highlight Fields Error!');
          console.warn(err);
        });

    };


    var none = function () {
      var cleanCurrentPage = function (done) {
        var currentIds = _PC.pagerIds.get(),
            sharedCollection = Pager.find({pagerId: {$in: currentIds}}); 
        sharedCollection.forEach(function (doc) {
          var pagerId = doc.pagerId,
              localCopy = LocalCol[_PC.ID].findOne({pagerId: pagerId}, {fields: {_id: 1}});
          var newDoc = _.omit(doc, '_id');
          LocalCol[_PC.ID].update(localCopy, newDoc);
        });
        done();
      };

      var cleanRest = function (done) {
        Meteor.setTimeout(function () {
          var selector = _PC.obvSelector.get(),
              sharedCollection = Pager.find(selector); 
          sharedCollection.forEach(function (doc) {
            var pagerId = doc.pagerId,
                localCopy = LocalCol[_PC.ID].findOne({pagerId: pagerId}, {fields: {_id: 1}});
            var newDoc = _.omit(doc, '_id');
            LocalCol[_PC.ID].update(localCopy, newDoc);
          });
          done();
        }, 0);
      };


      ASQ()
        .then(cleanCurrentPage)
        .then(cleanRest)
        .or(function (err) {
          console.warn('Splotlight Cleaner Error!');
          console.log(err);
        });

    };

    //*-----------------------------*/
    /// Highlight Reset
    /*-----------------------------*/
    var spotlightReconfig = function () {
      //Reset data
      if (_.isEmpty(spotlight) && settings.all) {
        none();
      }else{
        var spotlightFields = function (currentIds) {
          //Timeout needed????????????
          //
            var convertBack = function (done, id, newFields) {
              // console.log(arguments)
              // console.log(id)
              newFields = JSON.stringify(newFields);
              // console.log(newFields)
              var converted = redis[_PC.ID].set(id, newFields);
              // console.log(redis[_PC.ID].get(id))
              if (converted) {
                done();
              }
            };

            return ASQ()
              .val(configure)
              .then(function reconFig (done, funk) {
                var sq = ASQ();
                sq
                  .map(currentIds, function (id, done) {
                    var fields = JSON.parse(redis[_PC.ID].get(id)),
                        newFields = funk(fields);
                    return convertBack(done, id, newFields);
                  })
                  .val(function completeReconFig() {
                    //Parent Done
                    return done();
                  });
              });

        };

        var MGR = ASQ();
        MGR.defer()
          //Highlight current page first
          .then(function currentPage (done) {
            var currentIds = _PC.pagerIds.get();
            var sq = ASQ();
            sq
              .seq(spotlightFields(currentIds))
              .val(function currentPageComp () {
                //Parent Done
                return done();
              });
          })
          .then(function updateRender (done) {
            var sq = ASQ();
            sq
              .seq(_PC.renderDataGenerator())
              .val(function () {
                //parent done
                done();
              });
          })
          //And then the rest
          .then(function restOfPages (done) {
            if (_PC.spotlight.get().all) {
              debugger
            }else{
              done();
            }
          })
          .or(function (err){
            console.warn('Spotlight Reset Error!');
            console.warn(err);
          });  
      }

    };



    /*-----------------------------*/
    /// Main Manager
    /*-----------------------------*/
    var MGR = ASQ();
    return MGR.defer()
      .then(function resetNeeded(done) {
        if (fields === undefined) {
          spotlightReconfig();
          MGR.abort();
        }
        done();
      })
      .val(configure)
      .val(function (funk) {
        if (funk === null) {
          console.log('funk addCustomFields callled')
          //If user is only using 'addCustomFields'
          return fields;
        }else{
          return funk(fields);  
        }
      })
      .val(function (fields) {
        if (spotlight.addCustomFields || _.isObject(spotlight.customFields)) {
          var sq = ASQ();
          return sq
            .seq(customFields(fields));
        }
        return fields;
      })
      .val(function (fields) {
        return fields;
      });
  },

  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Auto Update
  // @return - description 
  /*--------------------------------------------------------------------------*/
  autoUpdateCheck: function () {
    var _PC = this,
        params = _PC.updateParams(),
        currentPage = params.currentPage,
        key = "p-"+currentPage;

    params.idsToCheck = _PC.catchedData.lrange(key, 0, -1);
    params.currentPage = currentPage;
    console.log(params)
    //Call
    Meteor.call('pagerPreFetchCheck', params, function (err, res) {
      if (err) {
        console.warn('Happy Pager AutoUpdateCheck Error!'); 
        console.error(err);
      }else{
        console.log('============================================')
        if (res.setNewPrefetch) {
          //Set current page ref
          _PC.pagerIds.set(res.newIdSet);
          if (_PC.preFetch) {
            //Clear old current prefetch refferance
            _PC.catchedData.ltrim(key, -1, 0);
            //Set new id list
            _.each(res.newIdSet, function (id) {
              _PC.catchedData.rpush(key, id);
            }); 
          }
        }


        //Check prefetch data
        if (_PC.autoUpdatePreFetch && !_PC.tempPreFetchCheckBlock.get()) {
          //Send keys to manager
          //???? Possibly exclude current page
          _PC.preFetchCheckManager(_PC.catchedData.keys('*'));
        }


      }
    });
    


  },




  cleanRedis: function () {
    console.log('inint')
    var catchedD = function (done) {
      var cdKeys = _PC.catchedData.redis.key('*');
      for (var i = 0; i < cdKeys.length; i++) {
        _PC.catchedData.redis.del(cdKeys[i]);
      }
      done();
    };

    var MGR = ASQ();
    return MGR
      .then(catchedD)
      .then(function done (done) {
        console.log('Redis Cleaned')
        done()
      })
      .or(function error (err) {
        console.warn('Redis Clean Error');
        console.error(err);
      }); 
  },
  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Cleans catch for specified pager instance
  // @return - removes docs from Pager, which then will be removed from Local
  // -> if resetData is true will 
  /*--------------------------------------------------------------------------*/
  cleanCatch: function (resetData) {
    var _PC = this,
        idsToScrub,
        pagesToScrub = [],
        options = {};
        options.fields = {pagerId: 1};

    var currentPage = _PC.currentPage.get(),
        catchedPages = _PC.pagesCatched.get(),
        // catchedData = _PC.catchedData.redis.matching("p-*").fetch(),
        catchedData = _PC.catchedData.get(),
        catchedDataList = _PC.catchedDataList.get();

    /**
     * Generates a list of those ids/pages to be scrubed from collection
     * @return -> Populates - idsToScrub and pagesToScrub arrays
     */
    function generateLists () {
      if (resetData) {
        throw 'Reset Neeeded';
      }
      //Maps out the current catchData
      //diff - diffrence between catchData page and user current page
      //index - index
      var mapCatch = function () {
        var catchSets = _.map(catchedData, function (val) {
          //Page number of catch set
          var catchPage = _.keys(val);
          catchPage = Number(catchPage[0]);
          //Only return if page is not listed in ensureCatch
          var ensureCatch = _.without(_PC.ensureCatch, true);
          if (!_.contains(ensureCatch, catchPage)) {
            //Calcs the page diffence since we want to remove furthest pages
            var diffrence = function (num1, num2) {
              return (num1 > num2)? num1-num2 : num2-num1;
            };
            return {
              dataVal: _.values(val)[0], 
              diff: diffrence(catchPage, currentPage),
              index: catchPage
            };
          }
        });
        catchSets = _.compact(catchSets);

        if (catchSets.length !== 0) {
          return _.compact(catchSets);
        }
        // throw 'Reset Neeeded';
      };

      //Sorts list from greatest diff to smallest
      var sortCatch = function (catchSets) {
        return _.sortBy(catchSets, function (val) {
          return -val.diff;
        });
      };

      // Splice off the sets to be cleaned
      var dataToClean = function (catchSets) {
        return _.first(catchSets, _PC.catchCleanerSize);
      };

      //Generates an array of ids to be scrubed and pushed 
      //its respective page number to pagesToScrub
      var genSrub = function (catchSets) {
        var idsToScrub =  _.map(catchSets, function (val) {
          var index = val.index;
          //Push-> page nums to be scrubed
          pagesToScrub.push(index);
          return val.dataVal;
        });
        return _.flatten(idsToScrub, true);
      };
      //Control->
      return ASQ()
        .val(mapCatch)
        .val(sortCatch)
        .val(dataToClean)
        .val(genSrub)
        .then(function (done, idSet) {
          //Set-> 
          idsToScrub = idSet;
          done();
        });
    }

    /**
     * Cleans Local Collection
     * @return -> Collection withouth srubId's
     */
    function cleanLocal (done) {
      var cleanCursor = LocalCol[_PC.ID].find({pagerId: {$in: idsToScrub}}, options);
        cleanCursor.forEach(function (doc) {
          LocalCol[_PC.ID].remove(doc._id);
        });
      done();
    }

    /**
     * Cleans Local Collection
     * @return -> Collection withouth srubId's
     */
    function cleanPager (done) {
      options.cleanAll = false;
      Meteor.call('pagerCleaner', idsToScrub, options, function (err, res) {
        if (err) {
          console.warn(err);
        }else{
          done();
        }
      });
    }

    /**
     * Cleans Client Refferances
     * @return -> updated reactive vars
     */
    function cleanClient () {
      //Generate Catched Data to be cleaned
      var genData = function (done) {
        var tempData = _.map(catchedData, function (val) {
          var index = _.keys(val);
          if (!_.contains(pagesToScrub, Number(index[0]))) {
            return val;
          }
        });
        done(_.compact(tempData));
      };
      //Generate Catched Pages to be cleaned
      var genPages = function (done) {
        var tempPages = _.map(catchedPages, function (val) {
          if (!_.contains(pagesToScrub, val)) {
            return val;
          }
        });
        //Check for zero, in which case we have to add back
        //on due to the nature of compact
        if (_.contains(tempPages, 0)) {
          tempPages = _.compact(tempPages);
          tempPages.push(0);
        }else{
          tempPages = _.compact(tempPages);
        }
        done(_.sortBy(tempPages, function (val) {
          return val;
        }));
      };
      //Generate Array of IDs to be cleaned
      var genDataList = function (done) {
        var tempCatchDataList = _.map(catchedDataList, function (val) {
          if (!_.contains(idsToScrub, val)) {
            return val;
          }
        });
        done(_.compact(tempCatchDataList));
      };
      //Control->
      return ASQ()
       .gate(
          genData,
          genPages,
          genDataList
        )
       .val(function (data, pages, dataList) {
          //Set-> vars with new info
          _PC.catchedData.set(data);
          _PC.pagesCatched.set(pages);
          _PC.catchedDataList.set(dataList);
       });
    }

    /**
     * Removes old data from collection
     * If the user refreshes the collection data will still remain but not
     * the refferances to the data thus making it void
     * One solution whould be to implement localStorage although it might bottleneck
     * @return -> Cleans local/shared collection
     */
    function removeOldData () {
      console.log('SSSSSSSSSSSSSSSSSSSSSSSSSHITR')
      //Generates ids
      var genIdsNotToScrub = function () {
        var catchedData = _PC.catchedData.get(),
            catchedIds = _.map(catchedData, function (doc) {
              var val = _.values(doc);
              return val;
            });
        catchedIds = _.flatten(catchedIds);
        return catchedIds;
      };
      //Cleans Local Collection
      var cleanLocal = function (idsNotToScrub) {
        var cleanCursor = LocalCol[_PC.ID].find({pagerId: {$nin: idsNotToScrub}}, options);
        cleanCursor.forEach(function (doc) {
          LocalCol[_PC.ID].remove(doc._id);
        });
      };
      //Cleans Shared Collection
      var cleanPager = function (idsNotToScrub) {
        var options = {};
        options.cleanOld = true;
        Meteor.call('pagerCleaner', idsNotToScrub, options);
      };

      //Contorll ->
      return ASQ()
       .val(genIdsNotToScrub)
       .val(function (ids) {
         var sq = ASQ();
           sq
             .gate(
               cleanLocal(ids),
               cleanPager(ids)
              )
             .or(function (err) {
               console.warn(err);
             });
          //End ->
          sq.abort();
       })
       .then(function (done) {
         done();
       });
    }

    //Origin Contorll ->->->
    var MGR = ASQ();
    MGR
      .seq(generateLists)
      .gate(
        cleanLocal,
        cleanPager
      )
      .seq(cleanClient)
      .then(function (done) {
        //Set ->
        _PC.cleanComplete.set(true);
        done();
      }).or(function (err, done) {
        //If no data to be cleaned, we need to do a hard clean on the collection
        if (err === 'Reset Neeeded') {
          var sq = ASQ();
          sq
           .seq(removeOldData)
           .then(function (done) {
              //Set ->
             _PC.cleanComplete.set(true);
             done();
           })
           .or(function(err){
              console.warn(err);
           });
        }else{
          console.warn(err);
        }
      });

  },

  /**
   * Updates
   * @param  {boolean} resetCatch - if true will bypass and reset catch (hard update)
   * otherwise it will proceed as normal, preFetching
   * @return Once updated it will procceed to call preFetch
   */
  update: function (resetCatch) {
    // console.log('UPDATE');
    var _PC = this,
        MGR = ASQ(),
        params = _PC.updateParams();


    /**
     * Determins if the catch will be used
     * @return - {boolean} useCatch - if true catch will be use 
     * @return - [id's] - ids ref list to be used
     */
    var checkCatch = function (resetCatch) {
      // debugger
      var catchSet;
      _PC.useCatch.set(false);
      //This var is specified in update call
      resetCatch = resetCatch === undefined ? false : true;

        if (!resetCatch) {
          var key = "p-"+_PC.currentPage.get(),
              exsist = _PC.catchedData.exists(key);
          if (exsist) {
            catchSet = _PC.catchedData.lrange(key, 0, -1);
            _PC.useCatch.set(true);
          }
        }else{
          debugger;
          //Hard upate reset all catched data since no longer reflective/actucate
          _PC.catchedDataList.set([]);
          _PC.catchedData.set([]);
          _PC.pagesCatched.set([]);
          //If update pre fetch is actice
          if (_PC.autoUpdatePreFetch) {
            //Set temp block since ids will be in flux and will produce a false/postive
            _PC.tempPreFetchCheckBlock.set(true);
          }
        }

      return catchSet;
    };


    var updateCall = function (done, catchSet) {
      var useLocalCatch = _PC.useCatch.get();
      _PC.isUpdateComplete.set(false);
      //Use local catch if set and avalible
      if (useLocalCatch) {
        console.log('USE CATCH===========>')
        //Set the ids to use for the catched data
        _PC.pagerIds.set(catchSet);
        //Set PAges
        _PC.setPages();
        if (_PC.preFetch) {
          //Fetches the next set
          _PC.catchPages(params);
        }
        _PC.isUpdateComplete.set(true);
        done();
      }

      //If no catch version is avalible
      if (!useLocalCatch) {
        console.log('NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO')
        _PC.isUpdateComplete.set(false);
        Meteor.call(_PC.method, params, function (err, res) {
          if (err) {
            console.log(err);
          }else{
            _PC.pagerIds.set(res.pagerId);
            _PC.totalRecords = res.totalRecords;
            _PC.filteredRecords = res.filteredRecords;
            _PC.setPages();
            _PC.isUpdateComplete.set(true);
            //Call-> preFetch / catch data
            var preFetchData = {
              currentPage: params.currentPage,
              pagerIds: res.pagerId,
            };
            _PC.catchPages(params, preFetchData);
            done();
          }
        });
      }
    };

    /**
     * Only called on first pass (init), ensurs proper load
     */
    var firstPass = function (done) {
      //If it is the first pass through we must set pages after the result
      //otherwise the setPages is handled in the changePage funk
      //note: no need for range since first call limited
      Meteor.call(_PC.method, params, function (err, res) {
        if (err) {
          console.log(err);
        }else{
          // console.log(res)
          _PC.pagerIds.set(res.pagerId);
          _PC.totalRecords = res.totalRecords;
          _PC.filteredRecords = res.filteredRecords;
          //** Set Pages
          _PC.setPages();
          _PC.isUpdateComplete.set(true);
          _PC.initLoaded.set(true);

          done();
          //Call-> preFetch / catch data
          var preFetchData = {
            currentPage: params.currentPage,
            pagerIds: res.pagerId,
          };
          _PC.catchPages(params, preFetchData);
        }
      });
    };

    var initPass = _PC.pass <= 1;
    /**
     * Funk-Manager for update;
     * First time through, it will take the path of initPass which
     * relays its completion back to the init method. 
     * Additionaly, this method will alway call _PC.catchPages
     * to catch/store said data.
     */
    return MGR
      .then(function firstPassCheck (done) {
        if (initPass) {
          firstPass(done);
        }else{
          done();
        }
      })
      .then(function update (done) {
        if (initPass) {
          done();
        }else{
          var sq = ASQ();
          sq
            .val(checkCatch(resetCatch))
            .then(function (done, catchSet) {
              updateCall(done, catchSet);
            })
            .val(function () {
              //Parent done;
              done();
            });
        }
      })
      //Calls the renderDataGenerator method, to take said
      //data and send it to the client
      .then(function renderData (done) {
        if (initPass) {
          done();
        }else{
          _PC.renderDataGenerator();
          done();
        }
      })
      .or(function (err) {
        console.warn('hPager: Error in update call!');
        console.warn(err);
      });


  },

/**
 * (M) - Pre-fetches pages
 * @param  {obj} params
 */
  catchPages: function (params, preFetchData) {
    var _PC = this;

    /**
     * Initial Catch Check
     * catchPages, can be called with a data context to set with catchIds
     * so if it is this will set it
     */
    function initCatch () {
      if (params !== undefined && preFetchData !== undefined) {
        return ASQ()
          .then(function (done) {
            catchIds(done, preFetchData);
          });
      }
      return ASQ()
        .then(function (done) {
          done();
        });
    }

    /**
     * Establishes params
     * @return {params}
     */
    function establishParams () {
      //Set Params if not defined
      if (params === undefined) {
        params = _PC.updateParams();
      }
      return params;
    }

    /**
     * Stores/creates ref to catched ids.
     * This bugger is the fucking key to the who preFetch gig
     * -sets - catchedData - which holds an array ref or Id for said page for lookup
     * -sets - pagesCatched - which is a simple array of which pages are catched
     * 
     * @param  {obj} params for method call
     * @param  {res} preFetchData - res from a preFetch call
     * @return invokes preFetch to fetch
     */
    function catchIds (done, data, batchMode) {
      var preFetchActive = data !== undefined ? true : false;
          batchMode = batchMode !== undefined ? true : false;
      
      /**
       * Inserts the doc into redis
       * Adds ref to pages catched
       * @param  {[page number]} key
       * @param  {[idSet]} value 
       */
      var insertDoc = function (key, value) {
        var formateKey = "p-"+key,
            catchExsists = _PC.catchedData.exists(formateKey);
        //Edge case check if users get page happy can push more than one set
        if (!catchExsists) {
          //Push new ids to set
          _.each(value, function(id){
            _PC.catchedData.rpush(formateKey, id);
          });
          //Update pages catched
          var tempPagesCatched = _.union(_PC.pagesCatched.get(), key);
          _PC.pagesCatched.set(tempPagesCatched);
        }
      };

      //Batch Mode
      if (batchMode) {
        _.each(data.currentPage, function (pageNum) {
          var key = pageNum,
              value = data.pageIds.splice(0, _PC.pageSize.get());
          insertDoc(key, value);
        });
      }

      //Non-batchMode
      if (!batchMode) {
        var key = preFetchActive ? data.currentPage : _PC.currentPage.get(),
            value = preFetchActive ? data.pagerIds : _PC.pagerIds.get();
        insertDoc(key, value);
      }


        done();
    }
    

    /**
     * Generates a Queue List
     * @return - pushing queue to _PC.preFetchQueue
     */
    function generateQueue () {
      var currentCatch = _PC.pagesCatched.get(),
          currentPage = _PC.currentPage.get(),
          deviation = _PC.preFetchDeviation;
      //*-----------------------------*/
      /// Helpers
      /*-----------------------------*/
        /**
         * Helper- cleans list
         * @param  {[pages]} list of pages
         * @return {[pages]} -> Cleaned
         */
        var cleanList = function (list) {
          //Clean the list, so there is no duplicate of records
          var cleanedList = _.map(list, function (val) {
            if (!_.contains(currentCatch, val)) {
              return val;
            }
          });
          //If it has zero we got to do a bit of a walk around
          if (_.contains(cleanedList, 0)) {
            //Compact removes zero
            cleanedList = _.compact(cleanedList);
            //So lets add it back on
            cleanedList.push(0);
            return cleanedList;
          }
          return _.compact(cleanedList);
        };
        /**
         * Helper - Checks to see if pages have been catched
         * so that pages will not be quened that are already catched
         * @param  {[numbers]} array
         * @return {boolean} vals have to all be true to return false
         */
        var catchCheck = function (array) {
          var contain = _.map(array, function (val) {
            return _.contains(currentCatch, val);
          });
          return !_.every(contain, _.identity);
        };

        /**
         * Helper - Creates divation list - like standard divation
         * @param  {number} page
         * @param  {number} amount to divate by
         * @param  {1 or -1} num - create a positive of neg list
         * @return {[numbers]} 
         */
        var createDivation = function (page, amount, num) {
          var divationList = [];
          for (var i = 1; i <= amount; i++) {
            var next = i * num;
            //Make sure no neg or bigger then max
            if ((page + next) >= 0 && (page + next) <= _PC.maxPageNumber) {
              divationList.push(page + next);
            }
          }
          return divationList;
        };

      //*-----------------------------*/
      /// Main Function
      /*-----------------------------*/
      /**
       * Finds pages that need to be put in queue list
       * @return {[nums]} - Of pages that need to be fetched
       */
      var createPageQueueList = function () {
        var preFetchQueue = [];

        //Ensure catchList to be catched
        var ensureCatchList = function (done) {
          //Remove true out of array
          var ensureCatch = _.without(_PC.ensureCatch, true);
          //Check ensureCatch
          if (catchCheck(ensureCatch)) {
            var ensureCatchQueue = ensureCatch;
            ensureCatchQueue = cleanList(ensureCatchQueue);
            for (var i = 0; i < ensureCatchQueue.length; i++) {
              preFetchQueue.push(ensureCatchQueue[i]);
            }
          }
          done();
        };

        //Next Page's to be catched
        var nextList = function (done) {
          var nextList = createDivation(currentPage, deviation, 1);
          if (catchCheck(nextList) && !_PC.lastPage) {
            nextList = cleanList(nextList);
            for (var i = 0; i < nextList.length; i++) {
              preFetchQueue.push(nextList[i]);
            }
          }
          done();
        };

        //Previous Page's to be catched
        var prevList = function (done) {
          var prevList = createDivation(currentPage, deviation, -1);
          if (catchCheck(prevList) && !_PC.firstPage) {
            prevList = cleanList(prevList);
            for (var i = 0; i < prevList.length; i++) {
              preFetchQueue.push(prevList[i]);
            }
          }
          done();
        };

        var preFetchAll = function (done) {
          for (var i = 0; i < _PC.maxPageNumber; i++) {
            //No need to catch what is already catched
            if (!_.contains(currentCatch, i)) {
              if (_PC.preFetchAllBatch === 0) {
                preFetchQueue.push(i);
              }else{
                //preFetchAllBatchs
                if (_PC.preFetchAllBatch >= preFetchQueue.length) {
                  preFetchQueue.push(i);
                }
              }
            }
          }
          done();
        };

        //Removes duplicates
        var removeDuplicates = function () {
          preFetchQueue = _.uniq(preFetchQueue);
          return preFetchQueue;
        };
        //Sub-Funk Call ->
        return ASQ()
          .then(function (done) {
            if (_PC.preFetchAll && _PC.pass >= 2) {
              preFetchAll(done);
            }else{
              var sq = ASQ();
              sq
                .gate(
                  ensureCatchList,
                  nextList,
                  prevList
                )
                .then(function () {
                  //Parent -> done
                  done();
                });
            }
          })
          .val(removeDuplicates)
          .or(function (err) {
            console.warn('hPager: Error in Pre-Fetch!');
            console.warn(err);
          });
      };
      //Funk Call ->
      return ASQ()
        .seq(createPageQueueList);
    }


    /**
     * Gets/Sets preFetch data
     * @param  {[pageList]} pageList to be queue/preFetched
     */
    function fetchQueue (queueList) {
        //Method call to server
        var fetchDataFromServer = function (done, currentPage, batchMode) {
          params.currentPage = currentPage.get();
          if (batchMode) {params.batchMode = true};
          Meteor.call(_PC.method, params, done.errfcb);
        };
        //Processes calls, and sets preFetch data
        var processQueue = function (queueList, batchMode) {
          if (!batchMode) {
            var queueLength = queueList.length;
          }
          //Call ->
          var MGR = ASQ();
          return MGR
            .then(function queueManager(done) {
              if (batchMode) {
                done(); 
              }else{
                var sq = ASQ();
                //Iterates through queueList, and gets/sets data
                _.each(queueList, function (pgNum, index) {
                  //Call ->
                  sq
                    //Get data
                    .then(function (done) {
                      fetchDataFromServer(done, pgNum);
                    })
                    //Sets data
                    .then(function (done, res) {
                      var data = {};
                      data.currentPage = pgNum;
                      data.pagerIds = res.pagerId;
                      //Sets local catch refferance
                      catchIds(done, data);
                    })
                    //Indicated completion
                    .val(function () {
                      if (index === queueLength-1) {
                        //Parent -> Done
                        done();
                      }
                    });
                });
              }
            })
            //Batch Quueue
            .then(function queueBatch (done) {
              if (!batchMode) {
                done();
              }else{
                var sq = ASQ();
                  sq
                    //Get data
                    .then(function (done) {
                      fetchDataFromServer(done, queueList, batchMode);
                    })
                    //Set Data
                    .then(function (done, res) {
                      var data = {};
                      data.currentPage = queueList;
                      data.pageIds = res.pagerId;
                      //Set catch
                      catchIds(done, data, batchMode);
                    })
                    //Indicated completion
                    .then(function comp () {
                      //Parent -> Done
                      done();
                    });
              }
            });
        };
        
        //Funk Call ->
        return ASQ()
          //Type of queue
          .then(function (done) {
            if (_PC.pass === 1 || !_PC.preFetchBatch) {
              done(false);
            }else{
              queueList = _.sortBy(queueList, function (num) {
                return num;
              });
              //Batch Trigger
              //Once we hit said limit, batch q
              if (queueList.length >= _PC.preFetchBatchSize) {
                done(true);
              }
            }
          })
          //Proccess Queuee
          .then(function (done, batchMode) {
            var sq = ASQ();
              sq
              .seq(processQueue(queueList, batchMode))
              .then(function () {
                //parend done
                done();
              });
          })
          //PrefetchALL batch check
          .then(function preFetchAllBatch (done, batchMode) {
            if (_PC.preFetchAllBatch !== 0 && _PC.pass >= 2) {
                /**
                 * Loops through batches untill all are preFetched
                 */
                var nextBatch = function () {
                  var MGR = ASQ();
                  return MGR
                    .seq(generateQueue)
                    .then(function preFetchLoop (done, queueList) {
                      //Queue till there no more
                      if (queueList.length !== 0) {
                        var sq = ASQ();
                        return sq
                          ///////////////////////////////////////Set defualts on preFetchAll Batch
                          .seq(processQueue(queueList, true))
                          .val(function () {
                            //Keep on fetching till there ani't nothing left
                            Meteor.setTimeout(function () {
                              nextBatch();
                            }, _PC.preFetchAllBatchTimeIntv);
                          });
                        }else{
                          //preFetchLoop -> done
                          done();
                        }
                    })
                    .val(function comp() {
                      //nextBatch -> done
                      done();
                    })
                    .or(function (err) {
                      console.warn('hPager: Error in Pre-Fetch Batch!');
                      console.warn(err);
                    });
                  };

              //Keep on fetching till there ani't nothing left
              ASQ()
                .seq(nextBatch(done));

            }else{
              done();//preFetchAllBatch -> done
            }
          })
        .then(function (done) {
          //Set ->
          _PC.preFetchComplete.set(true);
          //Master -> done
          done();
        });
      }


    //Call-> Origin
    var MGR = ASQ();
    MGR.defer()
      .then(function init (done) {
        initCatch().pipe(done);
        if (!_PC.preFetch) {
          MGR.abort();
        }
      })
      .val(establishParams)
      .seq(generateQueue)
      .then(function (done, queueList) {
        if (queueList.length > 0) {
          //Set ->
          _PC.preFetchComplete.set(false);
          //Call->
          ASQ()
            .seq(fetchQueue(queueList))
            .then(function () {
              //Parent -> done
              done();
            });
        }else{
          //Set ->
          _PC.preFetchComplete.set(true);
          //Parent -> done
          done();
        }
      })
      .then(function comp (done) {
        //Maser -> done
        done();
      })
      .or(function (err) {
        console.warn('Pre-Fetch Error!');
        console.warn(err);
      });
  },

  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Reffrance Bellow
  /*--------------------------------------------------------------------------*/
  preFetchCheckManager: function (keys) {
    var _PH = this;
    
    var keyNumbers = _.map(keys, function (key) {
      return Number(key.replace( /^\D+/g, ''));
    });
    var getCurrent = function () {
      return _PH.preFetchCheckList.get();
    };

    ASQ()
      .val(keyNumbers)
      .val(function (keyNums) {
       return _.union(keyNums, getCurrent()); 
      })
      .then(function complete (done, pagesToCheck) {
        _PH.preFetchCheckList.set(pagesToCheck);
        done();
      })
      .or(function (err) {
        console.warn('PreFetchCheck Manager Error!');
        console.error(err);
      });
  },
  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Checks preFetched data sets and changes it if needed
  // Its origin lays within _PC.autoUpdateCheck.
  // From there the current _PC.catechedData is sent to _PC.preFetchCheckManager
  // which happns to be the funk righ above us. From 0there its added to a call list
  // which yeah I know what you are thinking. WTF is going on here. Well you can
  // rest asure I am not all sure if this is dumb or brilliant or somewhere in 
  // between. Nevertheless, there is a reason for this maddness. Back to the story.
  // This call list is in a autoTracker which is throttled thrus reducing uneeded
  // checks and does not run if we are actually doing some real preFetching.
  // Anyways, the whole reason we do this is becuase if a document is changed 
  // and now falls withing the preFetch perview but not the current page, 
  // the chances will be reflected in the Pager collection but not the local.
  // And since 
  // @return - description 
  /*--------------------------------------------------------------------------*/
  
  preFetchCheck: function (queueList) {
    var _PC = this;
    console.log('RAN')

    var createQueue = function () {
      return queueList = _.sortBy(queueList, function (num) {
        return num;
      });
    };

    var createBatchQueue = function () {
        var batchQueue = {},
            batchCount = 0;

        var generateOrderedList = function () {
          return queueList = _.sortBy(queueList, function (num) {
            return num;
          });
        };
      
        /**
         * Creates batches
         * @param  {[#]}
         * @return {
         *   batchList: sequance of numbers (cannot be missing a num)
         *   throwList: all the numbs of the list that did not make it in the
         *   batchList
         * }
         */
        var createBatch = function (list) {
          var batchList = [],
              throwList = [];
          if (list.length === 1) {
            return {
              batchList: list,
              throwList: []
            };
          }
          for (var i = _.first(list), j = i, q = 0; i < (j+list.length); i++, q++) {
             if (list[q] === i && q < _PC.preFetchCheckBatchSize) {
                batchList.push(list[q]);
             }else{
              throwList.push(list[q]);
             }
          }
          return{
            batchList: batchList,
            throwList: throwList
          };
        };

      /**
       * Inserst batches into batchQuene
       * If it is complete is false its will loop this whole process
       * again until there all nums are put into batches
       */
      var generateBatchQueue = function (loopList) {
        var batch = createBatch(loopList);
        if (!_.isEmpty(batch.batchList)) {
          batchQueue[batchCount] = batch.batchList;
          batchCount++;
        }

        if (!_.isEmpty(batch.throwList)) {
          //Remove pages that have a batch
          var modifiedList = _.map(loopList, function (val) {
            if(!_.contains(batch.batchList, val)){
              return val;
            }
          });
          return {
            complete: false,
            modifiedList: _.without(modifiedList, undefined)
          };
        }else{
          return {
            complete: true,
          };
        }
      };

      return ASQ()
        .val(generateOrderedList)
        .then(function (done, list) {
          //Loops funk depending on val
          var nextBatch = function (loopList) {
            //Set looplist
            loopList = _.isArray(loopList) ? loopList : list;
            //Loop
            var MGR = ASQ();
            return MGR
              .val(generateBatchQueue(loopList))
              .then(function loop (done, list) {
                if (!list.complete) {
                  nextBatch(list.modifiedList);
                }else{
                  done();
                }
              })
              .val(function complete () {
                //Parent -> Done
                done();
              });
          };

          ASQ()
            .seq(nextBatch(done, list));

        })
        .val(function complete () {
          return batchQueue;
        })
        .or(function (err) {
          console.warn(err);
        });

    };


    /**
     * Makes call to compare and share data sets
     * @param  {ob} dataToCheck - {page#, pagerIds}
     * @return {Callback}             [description]
     */
    function checkData (done, currentPage, batchMode) {
      var params = _PC.updateParams();
      //Set->
      var idsToCheck = _.map(currentPage, function (page) {
        var key = "p-"+page;
        return _PC.catchedData.lrange(key, 0, -1);
      });
      idsToCheck = _.flatten(idsToCheck);
      var pagesToCheck = _.map(currentPage, function (page) {
        return page;
      });
      pagesToCheck = _.flatten(pagesToCheck);
      params.idsToCheck = idsToCheck;
      params.currentPage = pagesToCheck;
      params.batchMode = batchMode;
      //Call---> 
      return Meteor.call('pagerPreFetchCheck', params, function (err, res) {
        if (err) {
          console.warn('PreFetch Data Check Error!');
          console.error(err);
        }else{

          //No need to set new prefetch
          if (!res.setNewPrefetch) {
            console.log('=================> No')
            done();
          }
          
          /*-----------------------------*/
          /// If preFetched data needs to be changed
          /*-----------------------------*/

          var insertDoc = function (key, newValue) {
            console.log('====================> CHANGE DATA');
            var formateKey = "p-"+key;
            //Clear old current prefetch refferance
            _PC.catchedData.ltrim(formateKey, -1, 0);
            //Set new id list
            _.each(newValue, function (id) {
              _PC.catchedData.rpush(formateKey, id);
            }); 
          };

          //BatchMode
          if (res.setNewPrefetch && res.batchMode) {
            _.each(res.currentPage, function (pageNum) {
              var key = pageNum,
                  value = res.newIdSet.splice(0, _PC.pageSize.get());
              insertDoc(key, value);
            });
            done();
          }

          //Non-BatchMode
          if (res.setNewPrefetch && !res.batchMode) {
            console.log('====================> CHANGE DATA');
            insertDoc(res.currentPage, res.newIdSet);
            done();
          }
        }
      });
    }

    ASQ().defer()
      //Set watch var
      .then(function (done) {
        _PC.preFetchCheckComplete.set(false);
        done();
      })
      .then(function queue (done) {
        //Batch Mode
        if (_PC.preFetchCheckBatch) {
          var sq = ASQ();
          sq
            .seq(createBatchQueue())
            .val(function (batchList) {
              return done(batchList, _PC.preFetchCheckBatch);
            });
        }

        //Non batch
        if (!_PC.preFetchCheckBatch) {
          return done(createQueue(), _PC.preFetchCheckBatch);
        }
      })
      .then(function queueManager (done, pageData, batchMode) {
        console.log(pageData)
        var sq = ASQ();
          return sq
            //Needed to map otherwise it will throw err
            .then(function initMap (done) {
              if (batchMode) {
                pageData = _.toArray(pageData);
                done(pageData); 
              }else{
                done(pageData);
              }
            })
            .map(function queueManager (pageData, done) {
              checkData(done, pageData, batchMode);
            })
            .val(function complete() {
              //Parent -> done
              done();
            });

      })
      .then(function complete (done) {
        //Reset list
        _PC.preFetchCheckList.set([]);
        _PC.preFetchCheckComplete.set(true);
        done();
      })
      .or(function (err) {
        console.warn(err);
      });
  }, 


  //Create a list of setting changes
  changeSetting: function (setting, newValue, extend) {
    var _PC = this,
        settingVal = _PC[setting],
        oldVal,
        changeAction,
        keepSetting,
        varType;

console.log(arguments)
    var errorHelper = function () {
      if (!_PC.override) {
        //Extending with an array
        console.warn('Extending with an array value which I will try to do but the it prbly will not turn out as planed. Stick to extending objects only for best result.');
        console.warn('If you would like to remove these error warnings you can add "override: true" to this specific instance');
      }
    };

    var config = function (done) {
      //Checks
      if (!_.isString(setting)) {
        throw 'A change in settings only accepts string values. Make is so to procced.'; 
      }
      if (settingVal === undefined) {
        throw 'Hmm, no setting called: '+setting+' was found. Are you use you spelled the setting right? Otherwise, it looks like you are out of luck since there is not much I can do with undefined';
      }

      //Special change types
      // if (setting === 'pageSize') {extendChange = false};
      if (setting === 'spotlight') {
        changeAction = 'spotlighter';
        //Stash settings
        keepSetting = _.pick(settingVal.get(), 'current', 'all', 'singlePage');
        //Using pairs since it disconnects the reffrnace to the recVar
        //maybe not the best way to do so but it works
        //used for the originalSpotlight
        oldVal = _.pairs(settingVal.get());
      }

      //Determins var type for change
      if (settingVal instanceof ReactiveVar) {
        varType = 'reactive';
      }else{
        varType = 'static';
      }

      
      done();
    };

    var isObject = function () {
      var check = _.map(arguments, function (val){
        var list = [];
        list.push(_.isObject(val));
        list.push(!_.isFunction(val));
        list.push(!_.isArray(val));
        return list;
      });
      return _.every(check, _.identity);
    };

    //Set-> new settings
    var setRective = function (done) {
      var valid = !_.isEmpty(newValue) || _.isNumber(newValue) || _.isBoolean(newValue);
      var clean = function () {
        if (_.isArray(oldSetting)) {
          settingVal.set([]);
        }else if (_.isObject(oldSetting)){
          settingVal.set({});
        }else{
          settingVal.set(undefined);
        }
      };

      //Extend if specified
      if (extend) {
        //Using pairs to disconnect ref
        var oldSetting = _.pairs(settingVal.get())
        //New value
        if (valid) {
          if (!isObject(oldSetting, newValue)) {
           errorHelper();
          }
          var newSetting = _.deepExtend(newValue, _.object(oldSetting));
          settingVal.set(newSetting);
          console.log(newValue)
        }else{
          //If new val empty assume to be cleaned
          clean();
        }
      }else{
        //Set new val
        if (valid) {
          settingVal.set(newValue);
        }else{
          //If new val empty assume to be cleaned
          clean();
        }
      }
      done();
    };

    //Set static var
    var setStatic = function (done) {
      var oldSetting = settingVal,
          valid = !_.isEmpty(newValue) || _.isNumber(newValue) || _.isBoolean(newValue);
      var clean = function () {
        if (_.isArray(oldSetting)) {
          _PC[setting] = [];
        }else if(_.isObject(oldSetting)){
          _PC[setting] = {};
        }else{
          _PC[setting] = undefined;
        }
      };

      if (extend) {
        //New value
        if (valid) {
          if (!_.isObject(oldSetting) || !_.isObject(newValue)) {
           errorHelper();
          }
          var newSetting = _.extend(oldSetting, newValue);
          _PC[setting] = newSetting;
        }else{
          //If new val empty assume to be cleaned
          clean();
        }
      }else{
        //New value
        if (valid) {
          _PC[setting] = newValue;
        }else{
          //If new val empty assume to be cleaned
          clean();
        }
      }
      done();
    }; 

    var actionCall = function (done) {
      
      //Reconfig spotlight
      if (changeAction === 'spotlighter') {
        var currentVal = settingVal.get(),
            currentPage = _PC.currentPage.get(),
            oldValObj = _.object(oldVal),
            withSettings = _.deepExtend(currentVal, oldValObj, keepSetting);
      
          //Alright its about to get a bit harry here
          //We need to keep a original copy and temp due to the singlePage
          //feture. Thus, we got to know what persists and what disapears.
          if (_PC.originalSpotlight) {
            //If singlePage, we do not want to apply to orignal since that
            //is going to be the val on the next page and if it for a single instancevv
            if (!newValue.singlePage) {
              var extendObj = _.deepExtend(newValue, oldValObj);
              _PC.originalSpotlight.set(extendObj);
            }
          }else{
            //Init, so we need to set the page, if the page chnages this will
            //be deleted, refer to the renderDataGenerator and checkout
            //the MGR to get a better idea what I be talking about
            if (!newValue.singlePage) {
              newValue.page = currentPage;
              _PC.originalSpotlight = new ReactiveVar(newValue);
            }
            
          }
          settingVal.set(withSettings);
          _PC.spotlighter();
        }
      
      done();
    };


    ASQ()
     .then(config)
     .then(function set (done) {
       if (varType === 'reactive') {
        setRective(done);
       }
      if(varType === 'static'){
        setStatic(done);
      }
     })
     .then(function action (done) {
      console.log(changeAction)
        if (changeAction) {
          actionCall(done);
        }else{
          done();
        }
     })
     .or(function (err) {
       console.warn('PageHappy: Change Setting Error!');
       console.warn(err);
     });
  },
  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Sets the pages
  // @return - new page cals, called on both soft and hard update
  /*--------------------------------------------------------------------------*/
  setPages: function () {
    // console.log('SET PAGES')
    var _PC = this,
        currentPage = _PC.currentPage.get();
    _PC.maxPageNumber = Math.ceil(_PC.totalRecords / _PC.pageSize.get()) - 1;
    _PC.multiplePages = _PC.maxPageNumber > 0 ? true : false;
    _PC.nextPage = currentPage + 1;
    _PC.prevPage = currentPage - 1;
    _PC.firstPage = currentPage === 0;
    _PC.lastPage = currentPage === _PC.maxPageNumber;
    
    //Reconfigures the last page to be catched
    if (_.last(_PC.ensureCatch) === true) {
      var initEnsure = _PC.initEnsureCatch;
      //Set back to defualt
      _PC.ensureCatch.length = 0;
      _PC.ensureCatch.push(initEnsure);
      _PC.ensureCatch = _.flatten(_PC.ensureCatch);
      //Add last page to ensureCatch
      _PC.ensureCatch.push(_PC.maxPageNumber);
      //Re-add boolean back on
      _PC.ensureCatch.push(true);
    }


    //First time through set pager to ready
    if (_PC.pass === 0) {
      _PC.pagerReady.set(true);
    }
    //Increase Pass
    _PC.pass++;
    if (_PC.autoUpdate) {
      Session.set('pagerAutoUpdate', _PC.pass);
    }

    //@-> So other funks can move along with their lifes
    return true;
  },

  //Called with true saves data
  destroy: function (deleteData) {
    var _PC = this,
        trackers = _PC.trackers,
        pagerId = _PC.ID,
        options = {};
    options.masterId = _PC.pagerMasterId;
    options.cleanAll = true;

    //Stops all trackers
    var dTrackers = function (done) {
      _.each(trackers, function (val) {
        val.stop();
      });
      done();
    };
    // //Cleans+del local collection
    // var dLocal = function (done) {
    //   if (!deleteData) {
    //     LocalCol[pagerId].remove({})
    //     delete LocalCol[pagerId];
    //     done();
    //   }
    //   done();
    // }
    //Removes ids from shared pager
    var dPager = function (done) {
      if (deleteData) {
        return Meteor.call('pagerDestroy', options, function (err, res) {
          if (err) {
            console.warn(err);
          }else{
            console.log(res);
            done();
          }
        });
      }
      done();
    };

    //Stops observer + del
    var dObserver = function (done) {
      pagerClientObserver[pagerId].stop();
      delete pagerClientObserver[pagerId];
      done();
    };

    ASQ()
      .gate(
        dTrackers,
        // dLocal,
        dPager,
        dObserver
      )
      .or(function (err) {
          console.warn('Destroy Happy Pager Error!');
          console.warn(err);
        });
  },
  /******************************************************************************/
  /******************************************************************************/
  // Events
  /******************************************************************************/
  /******************************************************************************/
  
  pageChange: function (number) {
    console.log('PAGE CHANGe')
    var _PC = this;
    _PC.pagerReady.set(false);
    _PC.currentPage.set(number);
    var setPages = _PC.setPages();
    if (setPages) {
      _PC.update();
    }
    //Waits for update to complete before setting pages
    //which in turn sets pagerReady
    Tracker.autorun(function () {
      var updateStatus = _PC.isUpdateComplete.get();
      if (updateStatus) {
        _PC.pagerReady.set(true);
      }
    });
  },

  renderDataGenerator: function () {
    console.log('RENDER DATA GENERATOR')
    var _PC = this,
        pagerIds = _PC.pagerIds.get();
    

    var formateData = function () {
      var getDoc = function (id, done) {
        var data =  redis[_PC.ID].get(id);
        //Edgecase Error protection
        if (data !== undefined) {
          var formateData = JSON.parse(data),
              spotlight = _PC.spotlight.get();
          // console.log(spotlight)
          if (spotlight.current) {
            return ASQ()
              .seq(_PC.spotlighter(formateData))
              .val(function (newFields) {
                return done(newFields);
              })
              .or(function (err) {
                console.warn('Current Spotlight Error!');
                console.warn(err);
              });
          }else{
            return done(formateData);
          }
        }
      };
      return ASQ()
        .map(pagerIds, getDoc)
        .val(function formateDataComplete (dataArray) {
          return dataArray;
        })
        .or(function (err) {
          console.warn('Formate Data Error!');
          console.warn(err);
        });
    };

    var cleanOrigSpot = function (done) {
      //If there is a temp spotlight we need to set it to the spotlight
      //since the user created a `singlePage` spotlight
      _PC.spotlight.set(_PC.originalSpotlight.get());
      //Delete its after it is set since we no longer need it.
      delete _PC.originalSpotlight;
      done();
    };

    var MGR = ASQ();
    return MGR
      .then(function origCheck (done) {
        if (_PC.originalSpotlight) {
          var currentPage = _PC.currentPage.get(),
              spot = _PC.originalSpotlight.get();
          //If the page changes, we need to reset spotlight to the original
          //and only carry over those who are notr singlePaged.
          if (currentPage !== spot.page) {
            cleanOrigSpot(done);
          }else{
            done();
          }
        }else{
          done();
        }
      })
      .seq(formateData)
      .val(function (dataArray) {
        return _PC.renderData.set(dataArray);
      })
      .or(function (err) {
        console.warn('Render Data Generator Error!');
        console.warn(err);
      });
    
  },

  render: function () {
    var _PC = this;
    return _PC.renderData.get();
  },

  subReady: function () {
    var _PC = this;
    if (_PC.initSetUpComplete.get()) {
      var pagerReady = _PC.pagerReady.get(),
          usingCatch = _PC.useCatch.get(),
          subReady = _PC.subHandle.ready(),
          initLoaded = _PC.initLoaded.get();

      if (pagerReady && (usingCatch || initLoaded || subReady)) {

        return true;
      }else{
        return false;
      }
    }
    return false;
  },
  get: function (value) {
    var _PC = this;
    if (_PC.initSetUpComplete.get()) {
        var subReady = _PC.subHandle.ready(),
            pagerReady = _PC.pagerReady.get(),
            usingCatch = _PC.useCatch.get(),
            initLoaded = _PC.initLoaded.get();

      if (pagerReady && (subReady || usingCatch || initLoaded)) {
        value = _PC[value];
        if (value instanceof ReactiveVar) {
          return value.get();
        }else{
          return value;
        }
      }
    }
    return false;
  },

  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Init Setup
  // @return ->  
  /*--------------------------------------------------------------------------*/
  initSetup: function () {
    var _PC = this;
    function checkDefaults () {
      if (!_PC.override) {
        //Collection check
        if (!(this[_PC.collection] instanceof Mongo.Collection) && _.isString(_PC.collection)) {
          console.warn('You must speficy what Mongo collection you would like to use....');
          //No Collection sepecified
          if (_PC.collection === undefined) {
            throw '....And currently, you have no Mongo Collection sepecified';
          }else{
            throw '....And '+_PC.collection+' is not a Mongo Collection!';
          }
        }

        //If user is using actuall  mongo ref
        if (!_.isString(_PC.collection)) {
          throw 'Please use speficy your Mongo Collection via a string rather then the refferance.';
        }

        //Pager ID
        if (_PC.pagerMasterId === undefined){
          throw 'You must speficy a pagerId';
        }

        if (_PC.catchLimit < 150){
          throw 'I do not recommend to set your catch under 150.';
        }

        if (_PC.ensureCatch.length > 5) {
          console.warn('')
        }
      }
    }

    function staticDefualts (done) {
      //Reffernace for what the initial ensure Catch was, used when 
      //the last page num is changed
      _PC.initEnsureCatch = _.clone(_.without(_PC.ensureCatch, true));
      
      //Remove Old observer if it exsist
      if (pagerClientObserver[_PC.ID] !== null) {
        // pagerClientObserver[_PC.ID] = null;
      }

      if (!_PC.override) {
        
        //CatchLimit
        //check to see if it is still at defualt
        if (_PC.catchLimit === 5000) {
          // _PC.catchLimit = (_PC.pageSize * (_PC.preFetchDeviation * 10));
        }

        //catch cleaner
        //Check to see if still at defualt
        if (_PC.catchCleanerSize === 2) {
          //Fourth of the catchLimit
          // _PC.catchCleanerSize = Math.floor((_PC.catchLimit*0.10) / _PC.pageSize);
        }

        //Clean on load otherwise collection gets backed up on refresh
        if (!_PC.autoUpdate) {
          // _PC.cleanCollectionOnLoad = true;
        }

        /*-----------------------------*/
        /// Spotlight
        /*-----------------------------*/
        if (_PC.spotlight.all === undefined) {
          _PC.spotlight.current = true;
        }
      }
      done();
    }

    /**
     * Cleans Collection if coll. limit is set
     * @return -> cleaned collections
     */
    function collectionDefaults () {
        var cleanPager = function  () {
          var options = {};
          options.masterId = _PC.pagerMasterId;
          options.cleanAll = true;
          return Meteor.call('pagerCleaner', [], options);
        };

        //Clean Local Pager
        var cleanLocal = function () {
          // console.log(redis[_PC.ID].matching("*").fetch())
          // return LocalCol[_PC.ID].remove({});
        };
        //Run both cleaners
        ASQ()
          .gate(
            cleanPager,
            cleanLocal
          ).or(function (err) {
            //Report Errors
            console.warn(err);
          });
    }


    function reactiveDefualts (done) {
      /*-----------------------------*/
      /// Observer
      /*-----------------------------*/
      _PC.obvSelector = new ReactiveVar();

      /*-----------------------------*/
      /// Page Data
      /*-----------------------------*/
      //The current page that the user is on
      var currentPage = _PC.currentPage;
      _PC.currentPage = new ReactiveVar(currentPage);

      //Set page size / convt to recVar
      var pageSize = _PC.pageSize;
      _PC.pageSize = new ReactiveVar(pageSize);

      _PC.obvSubLimit = new ReactiveVar(_PC.pageSize.get());

      /*-----------------------------*/
      /// Query
      /*-----------------------------*/
      var spotlight = _PC.spotlight;
      // if (spotlight.singlePage) {
      //   //Need to ensue settings stick
      //   var settings = _.pick(_PC.spotlight, 'all', 'current');
      //   _PC.originalSpotlight = new ReactiveVar(_.extend(settings, {init:true}));
      // }
      _PC.spotlight = new ReactiveVar(spotlight);

      /*-----------------------------*/
      /// Observer
      /*-----------------------------*/
      if (_PC.user) {
        var userId = Meteor.userId();
        _PC.obvSelector.set({$and: [
          {pagerMasterId: _PC.pagerMasterId},
          {userId: userId}
        ]});
      }else{
        //No users
        // _PC.obvSelector.set({pagerId: {$in: catchedDataList}});
        _PC.obvSelector.set({pagerMasterId: _PC.pagerMasterId});
      }

      // _PC.obvSubLimit.set(_PC.pageSize.get());

      /*-----------------------------*/
      /// Status - vars
      /*-----------------------------*/
      //Ready Helper for template
      _PC.pagerReady = new ReactiveVar(false);
      //Inidicated initial load data is ready to be sent
      //to the client, which is the ensureIndex data
      _PC.initLoaded = new ReactiveVar(false);
      //If update is complete
      _PC.isUpdateComplete = new ReactiveVar(false);
      //Determins if using catch data or not,
      //it also shorts the subReay funk when using catch
      _PC.useCatch = new ReactiveVar(false);
      //Keeps track of if cleaning catch
      _PC.cleanComplete = new ReactiveVar(true);
      

      /*-----------------------------*/
      /// Catch / PreFetch Vars 
      /*-----------------------------*/
      //PagerIds - hold the current pages id ref
      _PC.pagerIds = new ReactiveVar([]);

      //Pager Catched ID List 
      _PC.catchedDataList = new ReactiveVar([]);
      //List the pages that are currently catched
      _PC.pagesCatched = new ReactiveVar(null);
      //Hold id ref to catched pages
      // _PC.catchedData = new Miniredis.RedisStore;
      _PC.catchedData = new Miniredis.RedisStore;
      //Prefetch Status - true = complete
      _PC.preFetchComplete = new ReactiveVar(false);


      if (_PC.autoUpdatePreFetch) {
        //Will be the array of objs ids to check
        _PC.preFetchCheckList = new ReactiveVar([]);
        //Only true is preFetchCheck is completed
        _PC.preFetchCheckComplete = new ReactiveVar(true);
        //Temp block on check when ids in flux
        _PC.tempPreFetchCheckBlock = new ReactiveVar(false);
      }

      /*-----------------------------*/
      /// Render
      /*-----------------------------*/
      _PC.renderData = new ReactiveVar([]);

      done();
    }

    //*---------------------------------------------*/
    // Init - check/set up defualts
    /*---------------------------------------------*/
    function setDefaults () {
      return ASQ()
        .gate(
          staticDefualts,
          reactiveDefualts,
          function (done) {
            if (_PC.cleanCollectionOnLoad) {
              collectionDefaults();
            }
            done();
          }
        );
    }

    //Origin Control ->
    return ASQ()
      .then(function (done) {
        checkDefaults();
        done();
      })
      .seq(setDefaults)
      .or(function error(err) {
        console.warn('Setup Error!');
        console.error(err);
        console.warn('If you would like to remove these error warnings you can add "override: true" to this specific instance');
      });
  },
};


/******************************************************************************/
/******************************************************************************/
// Method Calls
/******************************************************************************/
/******************************************************************************/
Meteor.methods({
  //Removes ids from pager collection
  pagerCleaner: function (ids, options) {
    console.log('PAGE CLEANER METHOD')
    var cleanCursor;
    
      //removes old data
      if (options.cleanOld) {
        cleanCursor = Pager.find({pagerId: {$nin: ids}}, options);
        cleanCursor.forEach(function (doc) {
          Pager.remove(doc._id);
        });
        return true;
      }


      //For cleanup when catch limit is hit
      cleanCursor = Pager.find({pagerId: {$in: ids}}, options);
      cleanCursor.forEach(function (doc) {
        Pager.remove(doc._id);
      });
      return true;
  },
    pagerPreFetchCheck: function (params) {
      //Extract varibles from optiosn
      var collection = self[params.collection],
          selector = params.selector,
          options = {
            sort: params.sort || {},
            fields: {_id: 1},
            skip: params.currentPage * params.pageSize,
            limit: params.pageSize
        };

        var currentIdSet = collection.find(selector, options),
            idsToCheck = params.idsToCheck,
            newIdSet = currentIdSet.map(function (doc) {
              return doc._id;
            });

        var compareIdList = function (arr1, arr2) {
          if (arr1.length === arr2.length) {
            var matchPairs = [];
            for (var i = 0; i < arr1.length; i++) {
              matchPairs.push(arr1[i] === arr2[i]);
            }
            //Will only return true if all pairs match
            return _.every(matchPairs, _.identity);
          }else{
            return false;
          }
        };

        if (compareIdList(idsToCheck, newIdSet)) {
          return {
            setNewPrefetch: false
          };
        }else{
          return {
            setNewPrefetch: true,
            newIdSet: newIdSet,
            currentPage: params.currentPage
          };
        }
    },
});
