/******************************************************************************/
/******************************************************************************/
/*  Controller: Panginator 
/*  Template: /client/views/templateLocation
/******************************************************************************/
/******************************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
// Created
/*******************************/
Template.PageHappy_UI.created = function () {
  // this.pageSelection = new ReactiveVar(null);
  // this.pageLocation = new ReactiveVar({});
  // this.pagReady = new ReactiveVar(false);
  this.pager = this.data.context;
  this.multiplePages = new ReactiveVar(false);
  this.maxPages = new ReactiveVar();
};

/*******************************/
// Rendered
/*******************************/
Template.PageHappy_UI.rendered = function () {
  // var pager = this.data.context,
  //     self = this;
  // Tracker.autorun(function () {
  //   if (pager.get('multiplePages')) {
  //     self.multiplePages.set(true);
  //   }
  //   if (_.isNumber(pager.get('maxPageNumber'))) {
  //     console.log('MAX')
  //     self.maxPages.set(pager.get('maxPageNumber'));
  //   };
  // });
};

/***************************************************************/
/* Template Helpers */
/***************************************************************/
//
Template.PageHappy_UI.helpers({
  //Shows UI if needed
  pag_showUI: function() {
    Session.get('pagerAutoUpdate');
    var pager = Template.instance().pager,
        multiplePages = pager.get('multiplePages');
    if (multiplePages) {
        //Dropdown needs to be reactivated
        Meteor.setTimeout(function () {
          $('#pag_dropdownSearch')
          .dropdown({
            action: 'select'
            // action: function (value) {
            //   $('#pag_dropdownSearch').dropdown('set selected', value);
            // }
          }); 
        }, 100);
      return true;
    }else{
      return false;
    }
  },
  //Determins if sub is ready
  // subReady: function () {
  //   var pager = Template.instance().pager;
  //   console.log('SUBBB')
  //   console.log(pager.subReady())
  //   return true
  // }, 
  //Determins if UI should be limited
  pag_Size: function (value) {
    Session.get('pagerAutoUpdate');
    var pager = Template.instance().pager;
    return pager.get('maxPageNumber')+1 > value;
  },
  //Current page displate
  pag_currentPage: function () {
    Session.get('pagerAutoUpdate');
    var pager = Template.instance().pager,
        currentPage = pager.get('currentPage')+1;
    //Sets drop down value to reflect current page
    $('#pag_dropdownSearch').dropdown('set selected', currentPage);
    return currentPage;
  },
  //Determins what the last page
  pag_lastPage: function () {
    Session.get('pagerAutoUpdate');
    var pager = Template.instance().pager,
        lastPage = pager.get('maxPageNumber')+1;
    return lastPage;
  },
  //Creates array of all the pages for selector
  pag_pageList: function () {
    Session.get('pagerAutoUpdate');
    var pager = Template.instance().pager,
        pageList = [];

      var maxPages = pager.get('maxPageNumber');

      for (var i = 1; i <= (maxPages + 1); i++) {
        pageList.push(i);
      }

    return pageList;
  },
  //Displays the page number for the selector
  pag_pageNumber: function () {
    return this;
  },
  pag_disable: function () {
    var pager = Template.instance().pager,
        currentPage = pager.get('currentPage'),
        maxPageNumber = pager.get('maxPageNumber'),
        disabled = {};
    currentPage === 0 ? disabled.prev = 'disabled' : disabled.prev = '';
    currentPage === maxPageNumber ? disabled.next = 'disabled' : disabled.next = '';
    return disabled;
  },
  //Determins what pag tab is active and appends class
  pag_active: function (value) {
    var pager = Template.instance().pager,
        currentPage = pager.get('currentPage') + 1,
        lastPage = pager.get('maxPageNumber') + 1;

    if (currentPage === value) {
      return 'active';
    }
    if (currentPage === lastPage && value === 'last') {
      return 'active';
    }
    //prbly could make a better iffy
    if (currentPage !== lastPage && value === 'selector' && currentPage !== 1 && currentPage !== 2 && currentPage !== 3) {
      return 'active';
    }
  }
});


/***************************************************************/
/* Template Events */
/***************************************************************/
Template.PageHappy_UI.events({
  //Previous Page
  'click .pag-prev': function(e) {
    e.preventDefault();
    if (!$(e.currentTarget).hasClass('disabled')) {
      var pager = Template.instance().pager,
          currentPage = pager.get('currentPage');
      pager.pageChange(currentPage - 1);
    }
  },
  //Next Page
  'click .pag-next': function(e) {
    e.preventDefault();
    if (!$(e.currentTarget).hasClass('disabled')) {
      var pager = Template.instance().pager,
          currentPage = pager.get('currentPage');
      pager.pageChange(currentPage + 1);
    }
  },
  //Dedicated Page Button
  'click .pag-item': function (e) {
    e.preventDefault();
    var pager = Template.instance().pager,
        goToPage = e.currentTarget.getAttribute('data-pagenumber') - 1;
    pager.pageChange(goToPage);
  },
  //Page Selection
  'click .pag-pageSelection': function (e) {
    e.preventDefault();
    var pager = Template.instance().pager,
        goToPage = e.currentTarget.getAttribute('data-pagenumber') - 1;
    pager.pageChange(goToPage);
  },
});
