var Future = Npm.require('fibers/future'),
    self = this;

/*---------------------------------------------*/
// Publish
/*---------------------------------------------*/
Meteor.publish("pager", function(selector, sort, limit, fields) {
  var options = {};
  options.sort = sort || {};
  options.limit = limit || {};
  options.fields = fields || {};
  // console.log(selector)
  // console.log(options)
  return Pager.find(selector, options);
});

/*---------------------------------------------*/
// Helper
/*---------------------------------------------*/
function pagerAsync(cb, params){
  return cb(null, getPageHappy(params));
}


/*---------------------------------------------*/
// Methods
/*---------------------------------------------*/
Meteor.methods({
  getPagerAsync: function (params) {
    this.unblock();
    var fut = new Future();
    var bound_callback = Meteor.bindEnvironment(function (err, res) {
      if(err) {
        fut.throw(err);
      }  else {
        fut.return(res);
      }
    });
    pagerAsync(bound_callback, params);
    return fut.wait();
  },
  pagerDestroy: function (options) {
    this.unblock();
    //Cleans all with 'x' masterId - used in destroy call
    var cleanCursor = Pager.find({pagerMasterId: options.masterId});
    var cleaned = [];
    cleanCursor.forEach(function (doc) {
      cleaned.push(doc._id);
      Pager.remove(doc._id);
    });
    return cleaned;
  },
  //Removes ids from pager collection
  pagerCleaner: function (ids, options) {
    console.log('PAGE CLEANER METHOD')
    this.unblock();
    var cleanCursor;

      //removes old data used in removeOldData on client
      if (options.cleanOld) {
        cleanCursor = Pager.find({pagerId: {$nin: ids}}, options);
        cleanCursor.forEach(function (doc) {
          Pager.remove(doc._id);
        });
        return true;
      }

      //For cleanup when catch limit is hit
      cleanCursor = Pager.find({pagerId: {$in: ids}}, options);
      cleanCursor.forEach(function (doc) {
        Pager.remove(doc._id);
      });
      return true;
    },
    pagerPreFetchCheck: function (params) {
      this.unblock();
      //Extract varibles from optiosn
      var collection = self[params.collection],
          selector = params.selector,
          options = {
            sort: params.sort || {},
            fields: {_id: 1},
            skip: params.currentPage * params.pageSize,
            limit: params.pageSize
        };

        //BatchMode set
        if (params.batchMode) {
          options.skip = _.first(params.currentPage) * params.pageSize;
          options.limit = (params.currentPage.length * params.pageSize);
          console.log(options);
        }

        var currentIdSet = collection.find(selector, options),
            idsToCheck = params.idsToCheck,
            newIdSet = currentIdSet.map(function (doc) {
              return doc._id;
            });

        var compareIdList = function (arr1, arr2) {
          if (arr1.length === arr2.length) {
            var matchPairs = [];
            for (var i = 0; i < arr1.length; i++) {
              matchPairs.push(arr1[i] === arr2[i]);
            }
            //Will only return true if all pairs match
            return _.every(matchPairs, _.identity);
          }else{
            return false;
          }
        };

        if (compareIdList(idsToCheck, newIdSet)) {
          return {
            setNewPrefetch: false
          };
        }else{
          return {
            setNewPrefetch: true,
            newIdSet: newIdSet,
            currentPage: params.currentPage,
            batchMode: params.batchMode
          };
        }

    },
    // getCurrentPhSet: function (params) {
    //   //Extract varibles from optiosn
    //   var collection = self[params.collection],
    //       selector = params.selector,
    //       options = {
    //         sort: params.sort || {},
    //         fields: {_id: 1},
    //         skip: params.currentPage * params.pageSize,
    //         limit: params.pageSize
    //     };
    //     var idSet = collection.find(selector, options),
    //         count = idSet.count();
    //     idSet = idSet.map(function (doc) {
    //       return doc._id;
    //     });
    //   return {
    //     totalRecords: count,
    //     pagerId: idSet
    //   };
    // }
});








//Figure out a better way to handle observers
var observers = {};
var observerDuration = 30 * 60 * 10000;

this.getPageHappy = function (params) {
  //Extract varibles from optiosn
  var collection = this[params.collection],
      selector = params.selector,
      userId = this.userId || 'user',
      _pcId = params.pagerMasterId,
      options = {
        sort: params.sort,
        fields: params.fields,
        limit: params.pageSize
    };

    (function config () {
      if (params.queryRange) {
        console.log('QUUERYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY')
        //First pass randeId will be undefined, since there is
        //no need to range on first due to inharent limit
        if (params.lastRangeId !== undefined) {
          var range = {}, key, value,
              rangeSelector = {},
              fields = {},
              sortVal = _.first(_.values(options.sort)),
              sortKey = _.first(_.keys(options.sort));
          fields[sortKey] = 1;
          sortVal === 1 ? key = '$gt' : key = '$lt';
          value = Pager.findOne({pagerId: params.lastRangeId}, {fields: fields});
          range[key] = value[sortKey];
          rangeSelector[sortKey] = range;
          selector = _.extend(selector, rangeSelector);
        }
        //Limit Config
        if (params.batchMode) {
          options.limit = (params.currentPage.length * params.pageSize);
        }
      }
      console.log(selector)

      if (!params.queryRange) {
        console.log('SKKKKKKKKKKKKKKKKKKKKKKKKIP')
        options.skip = params.currentPage * params.pageSize;
        if (params.batchMode) {
          options.skip = _.first(params.currentPage) * params.pageSize;
          options.limit = (params.currentPage.length * params.pageSize);
        }
      }

    })();
  





  /**
   * This siaf, checks the current obserbers, and retitres them if they are
   * pass the observerDuration
   * @return {[type]} [description]
   */



var pagerId = [];
  //Observers 
  observers[_pcId] = collection.find(selector, options).observeChanges({
      addedBefore:function (id, fields) {
        //Important
        pagerId.push(id);

        var defualts = {
          pagerId: id,
          pagerMasterId: _pcId,
          userId: userId,
        };
        if (params.pagerTimestamp) {defualts.pagerTimestamp = params.pagerTimestamp};
        // if (!_.isEmpty(params.highlight)) {defualts.highlight = false};

        //Check to see if doc exsists to 
        var oldDoc = Pager.findOne({$and: [
          {pagerId: id},
          {pagerMasterId: _pcId},
          {userId: userId}
        ]});
        //Add doc if it does not
        if (oldDoc === undefined) {
          console.log('Added===================================')
          var newDoc = _.deepExtend(defualts, fields);
          Pager.insert(newDoc);
        }

      },
      changed: function (id) {
        console.log('Changed===================================')
        var originalDoc = collection.findOne(id);
        originalDoc = _.omit(originalDoc, '_id');

        var defualts = {
          pagerId: id,
          pagerMasterId: _pcId,
          userId: userId,
        };
        if (params.pagerTimestamp) {defualts.pagerTimestamp = params.pagerTimestamp};

        var updatedDoc = _.deepExtend(originalDoc, defualts); 
        //And Update
        Pager.update({pagerId: id}, updatedDoc);
      },
      removed: function (id) {
        console.log('REMOVEd---------------------------------');
        Pager.remove({pagerId: id});
      }
  });
  observers[_pcId].createdAt = +new Date();
  observers[_pcId]._pcId = _pcId;

  console.log('++++++++++++++++++++COUNT++++++++++++++++++')
  console.log(_.keys(observers).length);

  var totalRecords = collection.find(selector, options).count();

  return {
    totalRecords: totalRecords,
    filteredRecords: totalRecords,
    pagerId: pagerId
  };

};




  /**
   * This siaf, checks the current obserbers, and retitres them if they are
   * pass the observerDuration
   * @return {[type]} [description]
   */
  // (function () {
  //   //Removes duplicate observers
  //   //For Users via userID

  //   // var duplicateObv = _.map(observers, function (obv) {
  //   //   console.log('MAP')
  //   //   console.log(obv._pcId)
  //   //   if (obv._pcId !== null) {
  //   //     console.log('==================================');
  //   //     return obv._pcId;
  //   //   }
  //   // });
  //   // duplicateObv = _.compact(duplicateObv);
  //   // //Stops and delets said observer
  //   // _.each(duplicateObv, function (id) {
  //   //   console.log('==============STOPED====================');
  //   //   var observer = observers[id];
  //   //   observer.stop();
  //   //   delete observers[id];
  //   // });

  //   // console.log(duplicateObv)
  //   //Checks, and if needed maps out the expired observers
  //   var retireObv = _.map(observers, function (obv) {
  //     if (+new Date() - obv.createdAt > observerDuration) {
  //       return obv._pcId;
  //     }
  //   });
  //   retireObv = _.compact(retireObv);
  //   //Stops and delets said observer
  //   _.each(retireObv, function (id) {
  //     console.log('777777777777777777777777777777777----Extired ')
  //     var observer = observers[id];
  //     observer.stop();
  //     delete observers[id];
  //   });
  // })();

//*---------------------------------------------*/
// Coutner
/*---------------------------------------------*/
// Meteor.publish('peopleCounter', function() {
//   Counts.publish(this, 'peopleCount', People.find());
// });

    // //Make a seperarte Method???????????
    // if (preFetchCheck) {
    //   console.log('PREFETCHCHECK')
    //   options.fields = {_id: 1};
    //   var currentIdSet = collection.find(selector, options),
    //       idsToCheck = params.idsToCheck; 

    //   var newIdSet = [];
    //   var idSetCheck = currentIdSet.map(function (idObj) {
    //     var id = _.values(idObj)[0];
    //     newIdSet.push(id);
    //     //If id does not fit, flag that shit
    //     if (!_.contains(idsToCheck, id)) {
    //       return false;
    //     }
    //     return true;
    //   });
    //   console.log('==========idSEt==============');
    //   console.log(newIdSet);
    //   console.log(idsToCheck);
    //   //Array must be all true to return true
    //   //If no change need will return false -- change need --> true
    //   var setNewPrefetch = !_.every(idSetCheck, _.identity);

    //   return {
    //     setNewPrefetch: setNewPrefetch,
    //     newIdSet: setNewPrefetch ? newIdSet : null,
    //     oldIdSet: setNewPrefetch ? idsToCheck : null,
    //     currentPage: params.currentPage
    //   };

    // }
