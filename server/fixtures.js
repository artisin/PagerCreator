Kadira.connect('F7asvpKdqc46dgWiK', 'c7216daa-27a3-44a5-8e92-2a2456bd0772')
Meteor.startup(function () {
  if (People.find().count() === 0) {
    
    var allIds = [];
    var counter = 0;
    var createFakeUser = function () {
      var name = fakeFixture.getName(),
          phone = function () {
            if (Math.round(Math.random()) === 0 ? true : false) {
              return fakeFixture.getPhone();
            }else{
              return null;
            }
          };
      counter++;
      var user = {
        number: counter,
        firstName: name,
        lastName: fakeFixture.getName(),
        email: {
          address: fakeFixture.getEmail(),
          verified: fakeFixture.getBollen()
        },
        phone: {
          cell: fakeFixture.getPhone(),
          work: phone(),
          home: phone(),
        },
        displayName: name,
        invitedSent: true,
        accountCreated: true,
        timestamp: fakeFixture.getRandomDate(new Date(2012, 0, 1), new Date()),
        profile: {
          location: fakeFixture.getLocation(),
          company: fakeFixture.getCompany(),
          bio: fakeFixture.getParagraph()
        },
      };

      if (arguments.length !== 0) {
        var modifyOptions = _.map(arguments, function (val) {
          return val;
        }),
        modifyUser = _.pick(user, modifyOptions);
        return modifyUser;
      }
      return user;
    };

    var peopleUsers = function (number) {
      counter = 0;
      for (var i = 0; i < number; i++) {
        var id = People.insert(createFakeUser('firstName', 'number', 'email'));
        allIds.push(id);
        console.log(i + '--User Created');
      }
    };
    peopleUsers(5000);

  }
});

Meteor.methods({
  createNewUsers: function (number) {
      var createFakeUser = function () {
        var name = fakeFixture.getName();
        return {
          number: Math.floor((Math.random() * 100) + 1),
          firstName: name,
        };
      };

      var initFakeUsers = function (number) {
        var user = createFakeUser();
        for (var i = 0; i < number; i++) {
          People.insert(user);
          console.log(user.number + '--User Created');
        }
      };
  }
});


var fakeFixture = {
  getPhone: function () {
    return Math.floor(1000000000 + Math.random() * 900000000);
  },
  getName: function() {
    var namesLength = this.names.length,
        name = this.names[Math.floor(Math.random() * namesLength)];
    return this.capitalize(name);
  },
  getWord:  function(min, max) {
    var length = this.wordLengths[Math.floor(Math.random() * 16)];
    if(min && (length < min)) length = min;
    if(max && (length > max)) length = max;
    var word = '';
    for(var i = 0; i < length; ++i) {
      var count = this.syllabeCounts[Math.floor(Math.random() * 16)];
      word += this.syllabes[Math.floor(Math.random() * count)];
    }
    return word;
  },
  getLocation: function () {
    var location = this.fromArray(this.cities);
    return {
      city: location[0],
      state: location[1]
    };
  },
  getSentence: function(length) {
    if(!length) {
      var length = 4 + Math.floor(Math.random() * 8);
    }
    var ending = (Math.random() < 0.95) ? '.' : (Math.random() < 0.5) ? '!' : '?';
    var result = this.getWord();
    result = result.slice(0,1).toUpperCase() + result.slice(1).toLowerCase();
    for(var i = 1; i < length; ++i) {
      result += ' ' + this.getWord();
    }
    return result + ending;
  },
  getParagraph: function(length) {
    if(!length) {
      length = 6 + Math.floor(Math.random() * 8);
    }
    var result = this.getSentence();
    for(var i = 1; i < length; ++i) {
      result += ' ' + this.getSentence();
    }
    return result;
  },
  getDomain: function() {
    return this.getWord(2) + this.domains[Math.floor(Math.random() * 8)];
  },
  getEmail: function () {
    return (this.getName() + '@' +this.getDomain()).toLowerCase();
  },
  getCompany: function () {
    var name = this.getWord(5, 12);
    return this.capitalize(name);
  },
  getRandomDate: function(start, end) {
    if (!start) {
        start = new Date(1900, 0, 1).getTime();
    } else {
        start = start.getTime();
    }
    if (!end) {
        end = new Date(2100, 0, 1).getTime();
    } else {
        end = end.getTime();
    }
    return new Date(start + Math.random() * (end - start));
    //return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  },
  getBollen: function () {
    return Math.round(Math.random()) === 0 ? true : false;
  },
  capitalize: function(str) {
    return str.slice(0,1).toUpperCase() + str.slice(1).toLowerCase();
  },
  fromArray: function(array) {
    return array[Math.floor(Math.random() * array.length)];
  },
  syllabes: [
  'the','ing','er','a','ly','ed','i','es','re','tion','in','e','con','y','ter','ex','al','de','com','o','di','en','an','ty','ry','u',
  'ti','ri','be','per','to','pro','ac','ad','ar','ers','ment','or','tions','ble','der','ma','na','si','un','at','dis','ca','cal','man','ap',
  'po','sion','vi','el','est','la','lar','pa','ture','for','is','mer','pe','ra','so','ta','as','col','fi','ful','get','low','ni','par','son',
  'tle','day','ny','pen','pre','tive','car','ci','mo','an','aus','pi','se','ten','tor','ver','ber','can','dy','et','it','mu','no','ple','cu',
  'fac','fer','gen','ic','land','light','ob','of','pos','tain','den','ings','mag','ments','set','some','sub','sur','ters','tu','af','au','cy','fa','im',
  'li','lo','men','min','mon','op','out','rec','ro','sen','side','tal','tic','ties','ward','age','ba','but','cit','cle','co','cov','daq','dif','ence',
  'ern','eve','hap','ies','ket','lec','main','mar','mis','my','nal','ness','ning','nu','oc','pres','sup','te','ted','tem','tin','tri','tro','up',
  ],
  wordLengths: [
    1, 1,
    2, 2, 2, 2, 2, 2, 2,
    3, 3, 3, 3,
    4, 4,
    5
  ],
  syllabeCounts: [
    10,15,20,25,
    30,35,40,45,
    50,75,100,125,
    150,175,175,175
  ],
  names: [
    'Abigail','Alice','Amelia','Angelina','Ann',
    'Ashley','Avery','Barbara','Brianna','Camila',
    'Chloe','Dorothy','Elizabeth','Ella','Emily',
    'Emma','Fiona','Florence','Gabrielle','Haley',
    'Hannah','Isabella','Jasmine','Jennifer','Jessica',
    'Juliette','Kate','Leah','Lily','Linda',
    'Lea','Madison','Makayla','Margaret','Maria',
    'Mariana','Mary','Megan','Mia','Olivia',
    'Patricia','Rachel','Samantha','Sarah','Sophie',
    'Susan','Taylor','Valeria','Victoria','Zoe',
    'Alexander','Anthony','Benjamin','Brandon','Carter',
    'Charles','Charlie','Christian','Christopher','Daniel',
    'David','Deven','Dylan','Elijah','Eric',
    'Ethan','Felix','Gabriel','George','Harry',
    'Hudson','Hunter','Jack','Jacob','James',
    'Jason','Jayden','Jeremiah','John','Joseph',
    'Joshua','Justin','Kevin','Liam','Logan',
    'Lucas','Matthew','Michael','Neil','Noah',
    'Oliver','Owen','Raphael','Richard','Robert',
    'Ryan','Samuel','Thomas','Tyler','William'
  ],
   domains: [
  '.net', '.org', '.edu', '.com',
  '.com', '.com', '.com', '.com',
  ],
  cities: [
    ['New York', 'New York'],
    ['Los Angeles', 'California'],
    ['Chicago', 'Illinois'],
    ['Houston', 'Texas' ],
    ['Philadelphia', 'Pennsylvania'] ,
    ['Phoenix', 'Arizona'],
    ['San Diego', 'California'],
    ['Dallas', 'Texas'],
    ['San Antonio', 'Texas' ],
    ['Detroit', 'Michigan'],
    ['San Jose', 'California'],  
    ['Indianapolis', 'Indiana'], 
    ['San Francisco', 'California'], 
    ['Jacksonville', 'Florida'], 
    ['Columbus', 'Ohio '], 
    ['Austin', 'Texas'], 
    ['Memphis', 'Tennessee'],  
    ['Baltimore', 'Maryland'], 
    ['Milwaukee', 'Wisconsin'],  
    ['Boston', 'Massachusetts'], 
    ['Charlotte', 'North Carolina'],
    ['El Paso', 'Texas'],  
    ['Washington', 'D.C.'],  
    ['Seattle', 'Washington'],
    ['Fort Worth', 'Texas'], 
    ['Denver', 'Colorado'],  
    ['Nashville-Davidson', 'Tennessee'], 
    ['Portland', 'Oregon'],  
    ['Oklahoma City', 'Oklahoma'],
    ['Las Vegas', 'Nevada'], 
  ]
};