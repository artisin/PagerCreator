# Meteor - old shell of what now is pageHappy

>Will update upon finishing



#####Highlight Query
-Must be an object
-If first 'contains: true' with use underscor
    -key, value pair
    -containsList: ['Tyler', 1, false]


##Spotlight
###What if does
If you specify a `spotlight` it will run the condition you specify and will append a `spotlight` field to the documents of the local collection with a boolean value. Essentually it is just transforming your local collection for your convinece. In addition, you can take it a step futher and actually transforme your local collection such as adding new fields or altering exsiting fields. Spotlighting does not alter the original collection in anyway since it is only altering the indiviual local collection. 

###Functionality
__Function__
`function` Specify a function that returns a `boolean`. By passing an argument to the function it will reffer to the indiviual document.
```javascript
```
* `null` by passing the name of the function with `null` it will remove 
* 
__Contains__
I have also implmented a Undersocre `_.contains` option for your convienence using either `contains` or `continsAll`
```javascript
```

__Custom__
You can also specify custom spolighting functions which give you a bit more flexiblity if you wish.
```javascript
```


###Setup
####Options
* You have two spotlighting options in terms of how it goes about its business
    - `current: [boolean]` (defualt) - Spotlighting is done on the fly only to current page and not to any preFetched pages. This helps conserve client resorces and as long as your spotlighting is not all to fast there should not be noticible load time diffrence.
    - `all: [boolean]` - Spotlighting is done to all pages including prefetched pages.
    - `session: name`: will return all pagerids  
* `singlePage: [boolean]` (defualt = false): if you would like to only spotlight a single page so when the user then procceds to the next or previous page for the spotlighting not to persist then set this it to `true`; This is useful in cases when you have spotlighting buttons and such. 
    - You cannot use this feture if you have `all` enabled.
    - It will not work if you put it in your PageHappy instance nor would you really want to. If you need such functionalty create a trigger to do so.
    - Also it is intelegent enough not to overright spotlighting that is not singlepaged that is if you are extending your spotlighting.
* `independant`
* `extend: [boolean]` --> Last Argument (defualt = false): when you change your spotlight and or you create a new spotlight all previous spotlighting conditions will not be applied. So if you spotlight all evens right of the bat and then had a event in which it would spotlight those whose first name is longer then six characters. Well when that event is activated only the six charachters condition will be applied not the original even condition. But lets say you wanted to extend your original when that event is activated so it would spotlight docs that meet both conditions, even and six characters. Well your in luck it is pretty easy just pass `true` as your last argument.

__Important__
* You can __not__ use both `contains` and `function` you must pick one. The reason being is `contains` is really just for convinence and you can create your own `contains` via a `function`. That being said, this does not apply to `custromFields` it will work with either or.

If you would like to spotlight off the bat, as in right away, on page load you specify a spotlight in your inital instance.
```javascript
Template.example.created = function () {
  this.pHappy = CreatePageHappy({
      //...
      //...
      spotlight: {
        //code here....
      }
  });
};
```

Alternativly, you can tie spotlighting to any type of event or trigger by using the `changeSetting` method on your `pHappy` instance. 
```javascript
Template.example.events({
  'click .spotlightOdd': function (e, tmpl) {
    e.preventDefault();
    var pHappy = tmpl.pHappy;
    pHappy.changeSetting('spotlight', {
      odd: function (doc) {
        return doc.number % 2 === 1;
      }
    });
  },
});
```

__Extending Example__
```javascript
Template.example.events({
  'click .spotlightSix': function (e, tmpl) {
    e.preventDefault();
    var pHappy = tmpl.pHappy;
    pHappy.changeSetting('spotlight', {
      six: function (doc) {
        return doc.name.length > 6;
      }
      //This is all your have to do
    }, true);
  },
});
```
As you prbly figured you can just keep snowballing these condition and there is no limit. Additionaly, you can alter an exsiting condition using in the same way.

#####Function Use
You need to pass an argument to the function which will be your reffernace to the individual mongo document, I recomend you use `doc` but you can use anything your like.
__Simple Example__ 
```javascrtipt
spotlight: {
  //This will obvously spotlight only evens
  even: function (doc) {
    return doc.email.verified === true;
  }
}
```